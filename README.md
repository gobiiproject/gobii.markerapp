
# ![pedver logo](docs/images/pedver_logo.png) F1 PedVer

### Gobii F1 Pedigree Verification application. 

`F1 PedVer` is a web application to help breeders to prepare data for creating flapjack project file.

`F1 PedVer's` goal is to aide breeders of different crops to achieve forward breeding and marker assisted backcrossing. 

Currently application performs following functions,

- Filter Genotype: Filters genotypes to by marker and sample call rate given by user.

- Consensus Calling: Performs consensus calling to get parent consensus for user provided consensus threshold.

- Split Genotypes: Splits input dataset using user selected split columns.

Flapjack project file will be available to download once split genotypes succeeds. 


### Application Architecture

The application has two components, Web application and an Analytics pipeline.

**Web Application** to get user input and to send jobs to the analytics pipeline.

**Analytics Pipeline** to run jobs for genotypes filtering, consensus calling and genotypes splitting.




![F1 Pedver Architecture](docs/images/PedVerArch.png)




##### - [To deploy locally](docs/deploy.md)

##### - [To setup development environment](docs/develop.md)


### Contact

- [http://cbsugobii05.biohpc.cornell.edu/wordpress/index.php/contact/](http://cbsugobii05.biohpc.cornell.edu/wordpress/index.php/contact/)

