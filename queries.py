
index_col = "consensusSampleIndex"

get_parent_consenus_samples = ("SELECT consensusSampleIndex "
                               "FROM consensus_samples")

consensus_by_name_template = ("SELECT * FROM consensus_{data_type}"
                              " WHERE consensusSampleIndex="
                              "'{consensus_sample_index}'")

replicates_by_name_template = "SELECT * FROM '{consensus_sample_index}_{data_type}'"

get_consensus_by_datatype_template = "SELECT * FROM consensus_{data_type}"


def get_consensus_by_index(consensus_sample_index, data_type):
    return consensus_by_name_template.format(
        data_type=data_type,
        consensus_sample_name=consensus_sample_index)


def get_replicates_by_index(consensus_sample_index, data_type):
    return hybrids_by_name_template.format(
        data_type=data_type,
        consensus_sample_index=consensus_sample_index)


def get_consensus_by_datatype(data_type):
    return get_consensus_by_datatype_template.format(data_type=data_type)
