#!/usr/bin/python

import os
from celery import Celery

import config

__celery_app_name__ = "pedver-tasks"

app_config = config.get_config()

# File path for all the data files used by the application
ROOT_FILE_PATH = app_config["APP_DATA"]["DATA_FILES_ROOT"]

TASK_DIR_TEMPLATE = os.path.join(ROOT_FILE_PATH, "{task_id}")

FLAPJACK_JAR = app_config["FLAPJACK"]["JAR_FILE"]


def make_celery():
    """ Creates a celery object by configuring it with broker url and
    backend from app config.
    """
    try:
        celery = Celery(
            __celery_app_name__,
            broker=app_config["CELERY"]["CELERY_BROKER_URL"],
            backend=app_config["CELERY"]["CELERY_RESULT_BACKEND"])
    except Exception:
        raise Exception("Unable to load celery")
    return celery


def get_task_dir(task_id):
    """ gets the task dir for given task id.
    created one if it does not exist.
    """
    try:
        task_dir = TASK_DIR_TEMPLATE.format(task_id=task_id)
        os.makedirs(task_dir, exist_ok=True)
    except Exception:
        raise Exception("not able to create task directory %s" % task_dir)
    return task_dir


USER_FIELD_TO_BRAPI_ADDITIONAL_FIELD_MAP = {
    "germplasmPar1": "par1",
    "germplasmPar2": "par2",
    "germplasmPedigree": "pedigree",
    "dnasampleSampleGroup": "sample_group",
    "dnasampleSampleGroupCycle": "sample_group_cycle"
}


class GENOTYPES_DATA_SPECS:
    """ Constants that define aspects of genotype
    data that will be used in the application.
    """
    SEPERATOR_CHAR = "/"
    MISSING_CHAR = "N"
    MISSING_VALUE = (MISSING_CHAR + SEPERATOR_CHAR + MISSING_CHAR)


class FILE_FORMAT:
    """ Constants the helps define input and output file formats.
    """
    FILL_NAN_CHAR = ""
    SEPERATOR_CHAR = "\t"
    LINE_TERMINATOR = "\n"
    COMMENT_IDENTIFIER = "#"
