#-----------------------------------------------------------------------------#

# removing current docker containers
docker rm -f gobiimarkerapp-worker gobiimarkerapp redis || true;
echo;

# Deleting images
docker image rm $(docker images) || true;
echo;

# Setting env params
HOST_DATA_VOLUME=data/pedver/
echo;

# Create directories for pedver data

mkdir -p ${HOST_DATA_VOLUME} || true;

mkdir -p ${HOST_DATA_VOLUME}data/ || true;

mkdir -p ${HOST_DATA_VOLUME}instance/ || true;

# Export vars

export CONTAINER_DATA_FOLDER=/data/
echo;

export CONTAINER_INSTANCE_FOLDER=/gobiimarkerapp/instance/
echo;

#-----------------------------------------------------------------------------#
### Docker Login

set +x # Turning down verbosity as the password is echoed to stdout
docker login -u $DOCKER_HUB_LOGIN_USERNAME -p $DOCKER_HUB_PASSWORD;
set -x
echo;

#-----------------------------------------------------------------------------#
### Start Container builds & deployments

# Pull redis from dockerhub
docker pull redis
echo;

# Deploy redis
docker run -dti \
-p 6379:6379 \
--name redis \
--restart=always \
redis
echo;

# Copy config.ini for container access

mkdir -p ${HOST_DATA_VOLUME}instance/
cp config.ini ${HOST_DATA_VOLUME}instance/
echo;

# Copy flapjack jar file to instance folder, so it is easy to just replace jars and restart
# containers when there is a flapjack update

cp -rf flapjackjars ${HOST_DATA_VOLUME}instance/ 
echo;

# Setting redis IP from docker network
#REDIS_IP=$(docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' redis)
#echo;

# set redis name
REDIS_NAME="gobii-pedver-redis"

# ! Running Python Script (What is this doing?)
python3 setup_config.py -i ${HOST_DATA_VOLUME}/instance/config.ini -r ${REDIS_NAME}
echo;

# Build app node
docker build -f gobiimarkerapp/Dockerfile -t gobiimarkerapp:0.5 .
echo;

# Build worker node
docker build -f celerytasks/Dockerfile -t gobiimarkerapp-worker:0.5 .
echo;

# Deploy app node
docker run -dti \
-p 80:80 \
-v /var/jenkins_home/workspace/pedver_build/${HOST_DATA_VOLUME}data:${CONTAINER_DATA_FOLDER} \
-v /var/jenkins_home/workspace/pedver_build/${HOST_DATA_VOLUME}instance:${CONTAINER_INSTANCE_FOLDER} \
--hostname gobiimarkerapp-web-node \
--restart=always \
--name gobiimarkerapp \
gobiimarkerapp:0.5
echo;

# Deploy worker node
docker run -dti  \
-v /var/jenkins_home/workspace/pedver_build/${HOST_DATA_VOLUME}data:${CONTAINER_DATA_FOLDER} \
-v /var/jenkins_home/workspace/pedver_build/${HOST_DATA_VOLUME}instance:${CONTAINER_INSTANCE_FOLDER} \
--hostname gobiimarkerapp-worker-node  \
--restart=always \
--name gobiimarkerapp-worker \
gobiimarkerapp-worker:0.5
echo;

# create and push docker images
docker commit -m "Image created for build tag: $BUILD_TAG" redis gadm01/gobii_pedver_redis:$BUILD_TAG

docker commit -m "Image created for build tag: $BUILD_TAG" gobiimarkerapp-worker gadm01/gobii_pedver_worker:$BUILD_TAG

docker commit -m "Image created for build tag: $BUILD_TAG" gobiimarkerapp gadm01/gobii_pedver_web:$BUILD_TAG

# Push new docker images
docker push gadm01/gobii_pedver_redis:$BUILD_TAG

docker push gadm01/gobii_pedver_worker:$BUILD_TAG

docker push gadm01/gobii_pedver_web:$BUILD_TAG
