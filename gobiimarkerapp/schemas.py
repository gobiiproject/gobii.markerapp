#!/usr/bin/python

from marshmallow import (Schema, fields, validate,
                         validates, ValidationError, EXCLUDE)


class HeaderSchema(Schema):
    ''' Schema for Request Headers.
    '''

    class Meta:
        unknown = EXCLUDE

    brapi_url = fields.String(data_key='X-Brapi-Baseurl', required=True)
    auth_token = fields.String(data_key='X-Brapi-Authtoken', required=True)


class FilterTaskSchema(Schema):
    ''' Schema for Filter task request body.
    '''

    class Meta:
        unknown = EXCLUDE

    variantset_ids = fields.List(fields.String, data_key="variantSetDbIds",
                                 required=True)

    marker_call_rate = fields.Float(data_key="markerCallRate", default=0,
                                    validate=validate.Range(min=0, max=1))

    sample_call_rate = fields.Float(data_key="sampleCallRate", default=0,
                                    validate=validate.Range(min=0, max=1))


class ConsensusCallingTaskSchema(Schema):
    ''' Schema for Consensus Calling task request body.
    '''

    class Meta:
        unknown = EXCLUDE

    variantset_ids = fields.List(fields.String, data_key="variantSetDbIds",
                                 required=True)

    filter_task_id = fields.String(data_key="filterTaskId")

    consensus_threshold = fields.Float(data_key="consensusThreshold",
                                       validate=validate.Range(min=0, max=1))


class SplitTaskSchema(Schema):
    ''' Schema for Splitting task request body.
    '''

    class Meta:
        unknown = EXCLUDE

    variantset_ids = fields.List(fields.String, data_key="variantSetDbIds",
                                 required=True)

    filter_task_id = fields.String(data_key="filterTaskId")
    consensus_task_id = fields.String(data_key="consensusTaskId")
    mapset_id = fields.String(data_key="mapDbId")
    split_columns = fields.List(fields.String, data_key="splitColumns")
    project_file_num_parents = (
        fields.Integer(default=-1,
                       data_key="projectFileNumOfParents"))


class PreviewTaskSchema(Schema):
    ''' Schema for preview data request body.
    '''

    class Meta:
        unknown = EXCLUDE

    variantset_ids = fields.List(fields.String, data_key="variantSetDbIds",
                                 required=True)

    mapset_id = fields.String(data_key="mapDbId")


class UpdateConsensusSchema(Schema):
    ''' Schema for updating consensus.
    '''

    class Meta:
        unknown = EXCLUDE

    markerName = fields.String(data_key="markerName", required=True)
    genotypeValue = fields.String(data_key="genotypeValue", required=True)
    parentSampleName = fields.String(data_key="parentSampleName", required=True)

    @validates("genotypeValue")
    def validate_genotype(self, value):
        allowed_genotypes = {"G", "C", "A", "T", ""}
        genotype_values = set(value.split("/"))
        if len(genotype_values - allowed_genotypes) > 0:
            raise ValidationError("invalid genotype value")
