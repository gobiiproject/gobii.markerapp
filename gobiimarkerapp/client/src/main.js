import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import vco from "v-click-outside";
import Vuelidate from "vuelidate";
import Keycloak from 'keycloak-js';

Vue.config.productionTip = false;
Vue.use(Vuelidate);
Vue.use(vco);

Vue.prototype.$kc = new Keycloak();

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount("#app");
