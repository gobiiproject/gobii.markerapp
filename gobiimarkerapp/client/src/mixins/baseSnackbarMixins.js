export const baseSnackbarMethods = {
  methods: {
    updateBaseSnackBarData: function(snackBarDetails) {
      this.errorOccurred = true;
      this.baseSnackBarMessage = snackBarDetails.errorMessage;
      this.baseSnackBarType = snackBarDetails.type;
      throw new Error(snackBarDetails.error);
    }
  }
};
