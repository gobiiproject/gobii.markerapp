export const downloadFileMethods = {
  methods: {
    downloadFile: function(objectURL, filename) {
      const fileURL = window.URL.createObjectURL(new Blob([objectURL]));
      let fileLink = document.createElement("a");
      fileLink.href = fileURL;
      fileLink.setAttribute("download", filename);
      document.body.appendChild(fileLink);
      fileLink.click();
    }
  }
};
