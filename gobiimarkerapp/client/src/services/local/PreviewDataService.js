import LocalApi from "./LocalApi";

export default {
  loadPreviewData(loadPreviewData) {
    const config = {
      headers: {
        "X-Brapi-AuthToken": localStorage.getItem("token"),
        "X-Brapi-BaseUrl": localStorage.getItem("brapiURL"),
        "Content-Type": "application/json",
      },
    };

    return LocalApi().post("/tasks", loadPreviewData, config);
  },

  getData(data) {
    return LocalApi().get(data);
  },
};
