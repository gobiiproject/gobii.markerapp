import LocalApi from "./LocalApi";

export default {
  downloadSplitData(splitDownload) {
    const config = {
      responseType: "blob"
    };
    return LocalApi().get(splitDownload, config);
  }
};
