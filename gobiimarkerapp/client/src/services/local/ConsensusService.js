import LocalApi from "./LocalApi";

export default {
  consensusCalling(filters) {
    const config = {
      headers: {
        "X-Brapi-AuthToken": localStorage.getItem("token"),
        "X-Brapi-BaseUrl": localStorage.getItem("brapiURL"),
        "Content-Type": "application/json",
      },
    };

    return LocalApi().post("/tasks", filters, config);
  },
  checkConsensusStatus(taskStatusRefLink) {
    return LocalApi().get(taskStatusRefLink);
  },
  getConsensusDataPreview(consensusPreview) {
    return LocalApi().get(consensusPreview);
  },
  updateConsensusTable(updateConsensusBody, consensusTaskId) {
    const config = {
      headers: {
        "X-Brapi-AuthToken": localStorage.getItem("token"),
        "X-Brapi-BaseUrl": localStorage.getItem("brapiURL"),
        "Content-Type": "application/json",
      },
    };

    const endPoint = `/consensus/${consensusTaskId}/parent-genotypes`;

    return LocalApi().patch(endPoint, updateConsensusBody, config);
  },
};
