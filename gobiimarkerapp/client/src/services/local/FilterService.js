import LocalApi from "./LocalApi";

export default {
  filterVariantset(filters) {
    const config = {
      headers: {
        "X-Brapi-AuthToken": localStorage.getItem("token"),
        "X-Brapi-BaseUrl": localStorage.getItem("brapiURL"),
        "Content-Type": "application/json",
      },
    };

    return LocalApi().post("/tasks", filters, config);
  },
  checkFilterStatus(taskStatusRefLink) {
    return LocalApi().get(taskStatusRefLink);
  },
  getFilteredDataPreview(filterPreview) {
    return LocalApi().get(filterPreview);
  },
};
