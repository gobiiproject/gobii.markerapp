import LocalApi from "./LocalApi";
import { auth } from "../auth/AuthApi";

export default {
  downloadData(dataType) {
    const config = {
      responseType: "blob",
    };
    return LocalApi().get(dataType, config);
  },

  downloadVariantset(variantDbId) {
    const config = {
      responseType: "blob",
    };
    const url = `brapi/v2/variantsets/${variantDbId}/calls/download`;
    return auth.get(url, config);
  },
};
