import axios from "axios";

let kcAuth = axios.create();

kcAuth.interceptors.request.use(config => {
  //console.log(config);
  return config;
});

kcAuth.interceptors.response.use(config => {
  //console.log(config);
  return config;
});

// for updating baseURL property upon login given a brapiURL
export function createKeyCloakAuthInstance(url) {
  return (kcAuth = axios.create({
    baseURL: url
  }));
}

// for updating the axios instance with the token retrieved upon login
export function updateAuthInstance(token) {
  kcAuth.interceptors.request.use(config => {
    // console.log(config);
    return config;
  });

  kcAuth.interceptors.response.use(config => {
    // console.log(config);
    return config;
  });

  // condition for login and logout
  if (token) kcAuth.defaults.headers["Authorization"] = `Bearer ${token}`;
  else kcAuth.defaults.headers["Authorization"] = null;
}

export { kcAuth };
