import { createAuthInstance } from "./AuthApi";

export default {
  loginDetail(credentials) {
    const auth = createAuthInstance(credentials.brapiurl);
    
    return auth.get("brapi/v2/serverinfo");
    
    // return auth.post("brapi/v1/token", {
    //   username: credentials.username,
    //   password: credentials.password,
    //   grant_type: "password",
    //   client_id: "pedver"
    // });
  }
};
