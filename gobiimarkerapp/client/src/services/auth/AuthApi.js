import axios from "axios";

let auth = axios.create();

auth.defaults.headers.common["Content-Type"] = "application/json";

auth.interceptors.request.use(config => {
  //console.log(config);
  return config;
});

auth.interceptors.response.use(config => {
  //console.log(config);
  return config;
});

// for updating baseURL property upon login given a brapiURL
export function createAuthInstance(url) {
  return (auth = axios.create({
    baseURL: url
  }));
}

// for updating the axios instance with the token retrieved upon login
export function updateAuthInstance(token) {
  auth.interceptors.request.use(config => {
    // console.log(config);
    return config;
  });

  auth.interceptors.response.use(config => {
    // console.log(config);
    return config;
  });

  // condition for login and logout
  if (token) auth.defaults.headers["Authorization"] = `Bearer ${token}`;
  else auth.defaults.headers["Authorization"] = null;
}

export { auth };
