import { auth } from "./AuthApi";

export default {
  downloadDataset(datasetId) {
    const url = `brapi/v2/variantsets/${datasetId}/calls/download`;
    return auth({
      url: url,
      method: "GET",
      responseType: "blob",
    }).then((res) => {
      return res;
    });
  },
  fetchStudies(url) {
    auth.defaults.baseURL = url;
    return auth.get("brapi/v2/studies");
  },
  fetchVariantsets() {
    return auth.get("brapi/v2/variantsets");
  },
  // Unused
  /* fetchDatasetsByStudy(selectedStudy) {
    return auth.get("brapi/v1/allelematrices", {
      params: {
        studyDbId: selectedStudy.studyDbId
      }
    });
  }, */
  fetchMapsets() {
    return auth.get("brapi/v2/maps");
  },
  fetchMapsetsByStudy(selectedStudy) {
    return auth.get("brapi/v2/maps", {
      params: {
        studyDbId: selectedStudy.studyDbId,
      },
    });
  },
  fetchMatrix(datasetId) {
    const url = `brapi/v2/variantsets/${datasetId}/calls`;
    return auth.get(url, {
      params: {
        pageSize: 100000,
      },
    });
  },
  fetchMarkerPositions() {
    return auth.get("brapi/v2/markerpositions");
  },
};
