import { createKeyCloakAuthInstance } from "./KeycloakApi";
import store from "@/store";

export default {

  getKeycloakToken(keycloakUrl, data) {
    
    const kcAuth = createKeyCloakAuthInstance(keycloakUrl);
    
    const config = {
      headers: { 'content-type': 'application/x-www-form-urlencoded' },
    };

    return kcAuth.post("realms/Gobii-Test/protocol/openid-connect/token", data, config);
  },

  getKeycloakAccount(keycloakUrl, token) {
    
    const kcAuth = createKeyCloakAuthInstance(keycloakUrl);

    const config = {
      headers: {
        'content-type': 'application/json',
        'Authorization': `Bearer ${token}` }
         
    };
   

    return kcAuth.get("realms/Gobii-Test/account", config);
  },

  logout(keycloakUrl,redirectUri) {
    
    const kcAuth = createKeyCloakAuthInstance(keycloakUrl);
    kcAuth.defaults.headers.common['Access-Control-Allow-Origin'] = redirectUri;
    // const url = `realms/Gobii-Test/protocol/openid-connect/logout?redirect_uri=${encodeURIComponent(redirectUri)}`;
    
    // const config = {
    //   headers: {
    //     'Access-Control-Allow-Origin':'*',
    //     'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'},
    //     'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    // };

    const url = "realms/Gobii-Test/protocol/openid-connect/logout"
    
    return kcAuth.get(url,{ params: { redirect_uri: redirectUri} });
  },
  
logoutRedirect(){
  const url = `${localStorage.getItem("keycloakUrl")}realms/Gobii-Test/protocol/openid-connect/logout?redirect_uri=${encodeURIComponent(store.state.redirectUri)}`;
  window.location.href = url;
  
  store.dispatch("setLogoutAction");
}
};
