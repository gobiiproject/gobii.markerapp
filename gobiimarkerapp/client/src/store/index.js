import Vue from "vue";
import Vuex from "vuex";
import router from "../router";

import { updateAuthInstance } from "@/services/auth/AuthApi";
import {
  SET_AUTH_USER,
  SET_BOTTOM_SHEET_DETAIL,
  SET_CONSENSUS_DETAIL,
  SET_CONSENSUS_PARENT,
  SET_CONSENSUS_DOWNLOAD_URL,
  SET_CONSENSUS_PREVIEW_URL,
  SET_CONSENSUS_PARENTS,
  SET_DATASET_ID,
  SET_KEYCLOAK_DETAIL,
  SET_MAP_ID,
  SET_DATASET_NAME,
  SET_MAP_NAME,
  SET_VARIANTSETS,
  SET_FILTER_DETAIL,
  SET_FILTER_DOWNLOAD_URL,
  SET_FILTER_PREVIEW_URL,
  SET_FILTER_RESULT,
  SET_PREVIEW_BRAPI_DATA_TABLE,
  SET_HIDE_ALERT,
  SET_SPLIT_DETAIL,
  SET_SPLIT_DOWNLOAD_URL,
  SET_SPLIT_STATISTICS_DATA,
  SET_SPLIT_WARNING_FOR_NO_CONSENSUS,
  DELETE_AUTH_USER,
  DELETE_SAVED_VARIABLES,
} from "./mutation-types";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    // Note: see mutations for state details
    accessToken: null,
    idToken: null,
    userId: null,
    userDisplayName: null,
    datasetId: null,
    datasetName: null,
    mapId: null,
    mapName: null,
    variantSets: [],
    filterDetail: null,
    filterPreview: null,
    filterDownload: null,
    code: null,
    consensusDetail: null,
    consensusParent: null,
    consensusPreview: null,
    consensusParents: [],
    consensusDownload: null,
    keycloakUrl: null,
    previewBrapiDataTable: {},
    redirectUri: null,
    splitDetail: null,
    splitStatisticsData: null,
    splitWarningForNoConsensus: null,
    server: null,
    splitDownload: null,
    filterResult: null,
    bottomSheet: {
      title: "bottom sheet",
      label: "release/2.2",
      type: "error",
    },
    bottomSheetShow: false,
  },

  mutations: {
    /**
     * MUTATIONS convention:
     *  Start the name with
     *    - 'ADD_' for adding new state elemnts;
     *    - 'SET_' for updating the values of state elements
     *    - 'DELETE_' / 'REMOVE_' for opposite of ADD
     *  Use mutation types.
     */

    [SET_AUTH_USER](state, userData) {
      state.idToken = userData.token;
      state.server = userData.server;
      state.userDisplayName = userData.userDisplayName;
      state.redirectUri =  userData.redirectUri;
    },

    [SET_KEYCLOAK_DETAIL](state, keycloakDetail) {
      state.accessToken =keycloakDetail.accessToken,
      state.idToken = keycloakDetail.idToken,
      state.refreshToken =keycloakDetail.refresToken,
      state.code= keycloakDetail.code,
      state.redirectUri= keycloakDetail.redirectUri
    },

    [DELETE_AUTH_USER](state) {
      state.accessToken = null;
      state.idToken = null;
      state.userId = null;
      state.userDisplayName = null;
      state.server = null;
    },

    [DELETE_SAVED_VARIABLES](state) {
      state.datasetId = null;
      state.variantSets = null;
      state.filterDetail = null;
      state.consensusDetail = null;
      state.splitDetail = null;
      state.splitDownload = null;
      state.splitStatisticsData = null;
      state.splitWarningForNoConsensus = null;
      state.consensusParents = [];
    },

    [SET_DATASET_ID](state, datasetId) {
      state.datasetId = datasetId;
    },

    [SET_MAP_ID](state, mapId) {
      state.mapId = mapId;
    },

    [SET_MAP_NAME](state, mapName) {
      state.mapName = mapName;
    },

    [SET_DATASET_NAME](state, datasetName) {
      state.datasetName = datasetName;
    },

    [SET_VARIANTSETS](state, variantSets) {
      state.variantSets = variantSets;
    },

    [SET_HIDE_ALERT](state) {
      state.bottomSheetShow = false;
    },

    [SET_BOTTOM_SHEET_DETAIL](state, bottomSheet) {
      state.bottomSheet = bottomSheet;
      state.bottomSheetShow = bottomSheet.show;
    },

    [SET_FILTER_DETAIL](state, filterDetail) {
      /** filterDetail contains the ff:
       * taskStatusRefLink = link to check the task status of the latest filter applied. value: /tasks/<taskId>/status
       * taskId = sample value: "b05a2539-092c-442a-86b4-099c42ecf7be"
       * sampleCallRate
       * markerCallRate
       */
      state.filterDetail = filterDetail;
    },

    [SET_CONSENSUS_DETAIL](state, consensusDetail) {
      /** consensusDetail contains the ff:
       * taskStatusRefLink = link to check the task status of the latest consensus calling. value: /tasks/<taskId>/status
       * taskId = sample value: "b05a2539-092c-442a-86b4-099c42ecf7be"
       * consensusThreshold
       */
      state.consensusDetail = consensusDetail;
    },

    [SET_CONSENSUS_PARENT](state, consensusParent) {
      state.consensusParent = consensusParent;
    },

    [SET_CONSENSUS_PARENTS](state, consensusSParents) {
      state.consensusParents = [...consensusSParents];
    },

    [SET_CONSENSUS_PREVIEW_URL](state, consensusPreview) {
      state.consensusPreview = consensusPreview;
    },

    [SET_CONSENSUS_DOWNLOAD_URL](state, consensusDownload) {
      state.consensusDownload = consensusDownload;
    },

    [SET_SPLIT_STATISTICS_DATA](state, splitStatisticsData) {
      state.splitStatisticsData = splitStatisticsData;
    },

    [SET_SPLIT_WARNING_FOR_NO_CONSENSUS](state, warning) {
      state.splitWarningForNoConsensus = warning;
    },

    [SET_FILTER_PREVIEW_URL](state, filterPreview) {
      state.filterPreview = filterPreview;
    },

    [SET_FILTER_DOWNLOAD_URL](state, filterDownload) {
      state.filterDownload = filterDownload;
    },

    [SET_FILTER_RESULT](state, filterResult) {
      state.filterResult = filterResult;
    },

    [SET_PREVIEW_BRAPI_DATA_TABLE](state, previewBrapiData) {
      state.previewBrapiDataTable = previewBrapiData;
    },

    [SET_SPLIT_DOWNLOAD_URL](state, splitDownload) {
      state.splitDownload = splitDownload;
    },

    [SET_SPLIT_DETAIL](state, splitDetail) {
      /** splitDetail contains the ff:
       * taskStatusRefLink = link to check the task status of the latest consensus calling. value: /tasks/<taskId>/status
       * taskId = sample value: "b05a2539-092c-442a-86b4-099c42ecf7be"
       * splitColumns
       */
      state.splitDetail = splitDetail;
    },
  },

  actions: {
    /**
     * ACTIONS convention:
     *  Start its name with the mutation name in camelCasing and
     *  the word '-Action' at the end.
     */

    deleteDatasetIdAction({ commit }) {
      commit("SET_DATASET_ID", null); //reset others
    },

    deleteVariantSetsAction({ commit }) {
      commit("SET_VARIANTSETS", null); //reset others
    },

    deleteMapAction({ commit }) {
      commit("SET_MAP_ID", null);
      commit("SET_MAP_NAME", null);
    },

    setDatasetIdAction({ commit }, selectedDataset) {
      commit("SET_DATASET_ID", selectedDataset.matrixDbId);
      commit("SET_DATASET_NAME", selectedDataset.name);
    },

    setMapAction({ commit }, selectedMap) {
      commit("SET_MAP_ID", selectedMap.mapDbId);
      commit("SET_MAP_NAME", selectedMap.name);
    },

    setVariantSetsAction({ commit }, variantSets) {
      commit("SET_VARIANTSETS", variantSets);
    },

    setFilterDetailAction({ commit }, filtersDetails) {
      commit("SET_FILTER_DETAIL", filtersDetails);
    },

    setFilterPreviewUrlAction({ commit }, filePreviewUrl) {
      commit("SET_FILTER_PREVIEW_URL", filePreviewUrl);
    },

    setFilterDownloadUrlAction({ commit }, fileDownloadUrl) {
      commit("SET_FILTER_DOWNLOAD_URL", fileDownloadUrl);
    },

    setConsensusDetailAction({ commit }, consensusDetails) {
      commit("SET_CONSENSUS_DETAIL", consensusDetails);
    },

    setConsensusParentAction({ commit }, consensusParent) {
      commit("SET_CONSENSUS_PARENT", consensusParent);
    },

    setConsensusParentsAction({ commit }, consensusParents) {
      commit("SET_CONSENSUS_PARENTS", consensusParents);
    },

    setConsensusPreviewUrlAction({ commit }, filePreviewUrl) {
      commit("SET_CONSENSUS_PREVIEW_URL", filePreviewUrl);
    },

    setConsensusDownloadUrlAction({ commit }, fileDownloadUrl) {
      commit("SET_CONSENSUS_DOWNLOAD_URL", fileDownloadUrl);
    },

    setPreviewBrapiDataTableAction({ commit }, previewBrapiData) {
      commit("SET_PREVIEW_BRAPI_DATA_TABLE", previewBrapiData);
    },

    setSplitStatisticsData({ commit }, splitStatisticsData) {
      commit("SET_SPLIT_STATISTICS_DATA", splitStatisticsData);
    },

    setSplitWarningForNoConsensus({ commit }, warning) {
      commit("SET_SPLIT_WARNING_FOR_NO_CONSENSUS", warning);
    },

    setSplitDetailAction({ commit }, splitDetail) {
      commit("SET_SPLIT_DETAIL", splitDetail);
    },

    setSplitDownloadUrlAction({ commit }, fileDownloadUrl) {
      commit("SET_SPLIT_DOWNLOAD_URL", fileDownloadUrl);
    },

    setAuthUserAction({ commit }, userData) {
      commit("SET_AUTH_USER", userData);
    },

    setKeycloakDetailAction({ commit }, keycloakDetail) {
      commit("SET_KEYCLOAK_DETAIL", keycloakDetail);
    },

    setHideAlertAction({ commit }) {
      commit("SET_HIDE_ALERT");
    },

    setBottomSheetDetailAction({ commit }, bottomSheet) {
      commit("SET_BOTTOM_SHEET_DETAIL", bottomSheet);
    },

    setFilterResultAction({ commit, state }, filterResult) {
      if (!state.idToken) {
        return;
      }
      commit("SET_FILTER_RESULT", filterResult);
    },

    setLogoutAction({ commit }) {
      const brapiURL = localStorage.getItem("brapiURL");
      let logoutlink = "/";
      if (brapiURL !== null) logoutlink += "?brapiurl=" + brapiURL;

      const bottomSheetDetail = {
        title: "",
        label: null,
        type: "Error",
        show: false,
      };
      commit("SET_BOTTOM_SHEET_DETAIL", bottomSheetDetail);
      commit("DELETE_AUTH_USER");
      commit("DELETE_SAVED_VARIABLES");

      updateAuthInstance(null);
      /**
       * Clearing the stored items in Local Storage except the BrAPI URL since
       * it will be used for displaying the history of used BrAPI URL in
       * Login component.
       */
      localStorage.removeItem("token");
      localStorage.removeItem("userDisplayName");
      localStorage.removeItem("keycloakUrl");
      localStorage.removeItem("redirectUri");

      router.replace(logoutlink);
    },

    setPageOnChangeAction({ commit }, page) {
      switch (page) {
        case "getData":
          commit("SET_FILTER_DETAIL", null);
          commit("SET_FILTER_RESULT", null);
        /* falls through */
        case "filter":
          commit("SET_CONSENSUS_DETAIL", null);
        /* falls through */
        case "consensus":
        /* falls through */
        default:
          commit("SET_SPLIT_DETAIL", null);
          commit("SET_SPLIT_STATISTICS_DATA", null);
          commit("SET_SPLIT_WARNING_FOR_NO_CONSENSUS", null);
          commit("SET_SPLIT_DOWNLOAD_URL", null);

          break;
      }
    },
  },

  getters: {
    /**
     * GETTERS convention:
     *  A getter should answer what am I returning.
     *  Start with 'is-' for returning a Boolean, or 'get-' otherwise
     */

    getConsensusParents(state, getters) {
      const consensusParentsNames = state.consensusParents.map(
        (consensusParent) => consensusParent.text
      );

      return getters.isConsensusCalled ? consensusParentsNames : null;
    },

    getDatasetName(state, getters) {
      return getters.isDatasetSelected ? state.datasetName : null;
    },

    getMapId(state, getters) {
      return getters.isMapsetSelected ? state.mapId : null;
    },

    getMapName(state, getters) {
      return getters.isMapsetSelected ? state.mapName : null;
    },

    getVariantSets(state, getters) {
      return getters.isVariantSetsSelected ? state.variantSets : null;
    },

    getVariantSetsIds(state, getters) {
      if (!getters.isVariantSetsSelected) return null;
      return state.variantSets.map((variantSet) => {
        return variantSet.matrixDbId;
      });
    },

    getVariantSetsNames(state, getters) {
      if (!getters.isVariantSetsSelected) return null;
      return state.variantSets.map((variantSet) => {
        return variantSet.name;
      });
    },

    isDatasetSelected(state) {
      return state.datasetId !== null;
    },

    isMapsetSelected(state) {
      return state.mapId !== null;
    },

    isFiltered(state) {
      return state.filterDetail !== null;
    },

    isFilteredWithResult(state) {
      return state.filterResult !== null;
    },

    isConsensusCalled(state) {
      return state.consensusDetail !== null;
    },

    isSplit(state) {
      return state.splitDetail !== null;
    },

    isVariantSetsSelected(state) {
      return state.variantSets !== null;
    },

    getMarkerCallRate(state, getters) {
      return getters.isFilteredWithResult
        ? state.filterDetail.markerCallRate * 100
        : null;
    },

    getPreviewBrapiDataTableHeader(state) {
      return state.previewBrapiDataTable.header;
    },

    getPreviewBrapiDataTableMatrix(state) {
      return state.previewBrapiDataTable.matrix;
    },

    getSampleCallRate(state, getters) {
      return getters.isFilteredWithResult
        ? state.filterDetail.sampleCallRate * 100
        : null;
    },

    getConsensusThreshold(state, getters) {
      return getters.isConsensusCalled
        ? state.consensusDetail.consensusThreshold * 100
        : null;
    },

    /* getConsensusParents(state, getters) {
      return getters.isConsensusCalled ? state.consensusParent : 0;
    }, */

    getSplitColumns(state, getters) {
      return getters.isSplit ? state.splitDetail.splitColumns : null;
    },

    getMarkersRemaining(state, getters) {
      return getters.isFiltered ? state.filterResult.markersRemaining : null;
    },

    getSamplesExcluded(state, getters) {
      return getters.isFiltered ? state.filterResult.samplesExcluded : null;
    },

    getMarkersExcluded(state, getters) {
      return getters.isFiltered ? state.filterResult.markersExcluded : null;
    },

    getSamplesRemaining(state, getters) {
      return getters.isFiltered ? state.filterResult.samplesRemaining : null;
    },
  },
});
