import Vue from "vue";
import Vuetify from "vuetify/lib";

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    dark: false,
    themes: {
      light: {
        primary: "#7CB342",
        accent: "#F06449",
        secondary: "#ADA9AF",
        success: "#40A186",
        info: "#44A0AD",
        warning: "#FB930E",
        error: "#D43A3A"
      }
    }
  },
  icons: {
    iconfont: "mdi"
  }
});
