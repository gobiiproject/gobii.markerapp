import Vue from "vue";
import Router from "vue-router";
import store from "./store";
import KeycloakService from "@/services/auth/KeycloakService";
import { updateAuthInstance } from "@/services/auth/AuthApi";

Vue.use(Router);

function guard(to, from, next) {
  if(localStorage.getItem("token")!== null){
    
    const token = localStorage.getItem("token");
    KeycloakService.getKeycloakAccount(localStorage.getItem("keycloakUrl"), token).then((kcTokenResponse) =>{
      let userDName=kcTokenResponse.data.username;

      const server = localStorage.getItem("brapiURL");
  
      store.dispatch("setAuthUserAction", {
        token: token,
        userDisplayName: userDName,
        server: server,
        redirectUri: localStorage.getItem("redirectUri")
      });
  
      updateAuthInstance(token);
  
      
     next();
    })
    .catch((error) =>{ // 401, existing token is no longer valid
        
      store.dispatch("setLogoutAction");
      next("/");
       
    });
  }else{
    next("/");
  } 
}

function goToLogin(to, from, next) {
  
  if(localStorage.getItem("token")!== null){ //check if already logged in

    next("/steps");
  }
  else if(to.fullPath.includes("code=")){ //if not, check if this is actually a redirect back from GDM keycloak
  
    const fullPath = to.fullPath;
    const pathParams = fullPath.split("&")

    let code = findCode(pathParams)
    if(code===""){
      next();
    }
    else{
      let redirectURI = ifCallbackPresentgetRedirectUri();

      const obj = {
          code: code,
          grant_type: 'authorization_code',
          client_id: 'gdm-client',
          redirect_uri: redirectURI
      };
        
      const data = Object.keys(obj)
      .map((key, index) => `${key}=${encodeURIComponent(obj[key])}`)
      .join('&');
          
          
      KeycloakService.getKeycloakToken(localStorage.getItem("keycloakUrl"), data).then((kcTokenResponse) =>{
          const keycloakDetail = {
            accessToken : kcTokenResponse?.data?.access_token,
            idToken: kcTokenResponse?.data?.id_token,
            refreshToken : kcTokenResponse?.data?.refresh_token,
            code: code,
            redirectUri: redirectURI

            /**
             * Other vaues of kcTokenResponse that wasn't included in the store
             * expires_in, not-before-policy, refresh_expires_in, refresh_token, scope, session_state, token_type
             */
          };
      localStorage.setItem("redirectUri",keycloakDetail.redirectUri);    
      localStorage.setItem("token",keycloakDetail.accessToken);
          store.dispatch("setKeycloakDetailAction", keycloakDetail);
          next("/steps");
      })
      .catch((error) =>{ 
          console.log(error)
          next();
                      
      });

    }
  }else{ //Just redirect to login page
    next();
  }
}

function findCode(paramValue) {
  var arrayLength = paramValue.length;

    for (var i = 0; i < arrayLength; i++) {
      if(paramValue[i].includes("code=")){
        const codeValue = paramValue[i].split("=")
        return codeValue[1];
      }
    }
 return "";
}

function ifCallbackPresentgetRedirectUri() {
  let redirectURI="";
  for (var i = 0; i <= localStorage.length - 1; i++) { // i < 100 works perfectly
    let k = localStorage.key(i);
    if(k.includes("kc-callback")){
      const value = JSON.parse(localStorage.getItem(k));
      redirectURI = value?.redirectUri;
      localStorage.removeItem(k);
    }
  }
  return decodeURIComponent(redirectURI);
}

const router = new Router({
  routes: [
    {
      path: "/",
      name: "login",
      component: () =>
        import(/* webpackChunkName: "login" */ "./views/Login.vue"),
      beforeEnter: goToLogin,
      meta: {
        title: "GOBii PedVer Login",
      },
    },
    {
      path: "/steps",
      name: "steps",
      component: () =>
        import(/* webpackChunkName: "steps" */ "./views/Steps.vue"),
      beforeEnter: guard,
    },
    {
      path: "*",
      component: () =>
      import(/* webpackChunkName: "login" */ "./views/Login.vue"),
      beforeEnter: goToLogin,
    },
  ],
});

const DEFAULT_TITLE = "GOBii CAST";
router.afterEach((to) => {
  document.title = to.meta.title || DEFAULT_TITLE;
});

export default router;
