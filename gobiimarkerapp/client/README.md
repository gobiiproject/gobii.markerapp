# CAST app

## Project setup

```console
npm install
```

### DevServer proxy for development

Enabling and configuring the `devServer.proxy` in `vue.config.js` will proxy API requests to an API server during development.
Set this to a remote CAST instance to use.

### Compiles and hot-reloads for development

```console
npm run serve
```

### Compiles and minifies for production

```console
npm run build
```

### Run your unit tests

```console
npm run test:unit
```

### Lints and fixes files

```console
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
