module.exports = {
  transpileDependencies: ["vuetify"],
  outputDir: "../static",
  assetsDir: "../static",
  indexPath: "../templates/index.html",
  chainWebpack: config => {
    config.module
      .rule('raw')
      .test(/\.txt$/)
      .use('raw-loader')
      .loader('raw-loader')
      .end()
  },
  // devServer: {
  //   proxy: 'http://cast.gdm.example.org:7000'
  // }
};
