#!/usr/bin/python
#!python


import inspect, logging

def log(message):
    """
    Referred from:
    ("https://stackoverflow.com/questions/10973362/"
     "python-logging-function-name-file-name-line-number-using-a-single-file")
    """
    func = inspect.currentframe().f_back.f_code
    logging.warning("%s: %s in %s:%i",
            message,
            func.co_name,
            func.co_filename,
            func.co_firstlineno
            ) 


