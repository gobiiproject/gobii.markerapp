#!/usr/bin/python

from flask import Flask
from flask import (
    request,
    render_template,
    redirect,
    url_for,
    Response,
    jsonify,
    send_file,
)
from flask_cors import CORS

from celery import Celery
import sqlite3 as sqlite
import pandas as pd

import os
import time
import json
from collections import OrderedDict

from marshmallow import ValidationError

import common

from gobiimarkerapp.schemas import (
    FilterTaskSchema,
    ConsensusCallingTaskSchema,
    SplitTaskSchema,
    HeaderSchema,
    UpdateConsensusSchema,
    PreviewTaskSchema,
)

# Initialize Flask Application
app = Flask(__name__)
CORS(app)

# Celery App
celery = common.make_celery()


TASK_STATUS_CALL = "/tasks/{task_id}/status"

CELERY_TASKS_SELECTOR = {
    "FILTER-GENOTYPES": "tasks.filter_task",
    "CONSENSUS-CALLING": "tasks.consensus_calling",
    "SPLIT-GENOTYPES": "tasks.split_genotypes",
    "LOAD-PREVIEW": "tasks.load_preview",
}

REQUEST_SCHEMA_SELECTOR = {
    "FILTER-GENOTYPES": FilterTaskSchema(),
    "CONSENSUS-CALLING": ConsensusCallingTaskSchema(),
    "SPLIT-GENOTYPES": SplitTaskSchema(),
    "LOAD-PREVIEW": PreviewTaskSchema(),
}


@app.route("/")
def main():
    return render_template("index.html")


@app.route("/favicon.ico")
def favicon():
    return redirect(url_for("static", filename="favicon.ico"))


@app.route("/tasks", methods=["POST"])
def insert_task():
    """Router for POSTing pedver tasks.
    returns:
        JSON object with,
        taskId: Id of the successfully  submitted task
        taskStatusRefLink: link to check the status of the task.
    """
    request_body = request.get_json()
    request_headers = dict(request.headers)
    task_type = request_body.get("taskType", None)

    if task_type in CELERY_TASKS_SELECTOR:

        try:
            task_args = REQUEST_SCHEMA_SELECTOR[task_type].load(request_body)
            task_args.update(HeaderSchema().load(request_headers))
        except ValidationError as err:
            return {"errors": err.messages}, 400

        task = celery.send_task(CELERY_TASKS_SELECTOR[task_type], kwargs=task_args)

        task_status_ref_link = TASK_STATUS_CALL.format(task_id=task.id)

        return jsonify({"taskId": task.id, "taskStatusRefLink": task_status_ref_link})

    return jsonify({"error": "Invalid task type"}), 500


@app.route("/tasks/<task_id>/status")
def get_status(task_id):
    """Gets the status of the task"""
    task = celery.AsyncResult(task_id)
    if task.state == "PENDING":
        return jsonify({"state": task.state})
    return jsonify(
        {
            "state": task.state,
            "status": task.info.get("message", ""),
            "completedTasks": task.info.get("completed_tasks", []),
        }
    )


@app.route("/files/<task_id>/<file_name>")
def get_file_by_taskid(task_id, file_name):

    try:
        root_file_path = common.app_config["APP_DATA"]["DATA_FILES_ROOT"]
        file_path = os.path.join(root_file_path, task_id, file_name)
        download_file_name = task_id.replace("-", "") + "_" + file_name

        if not os.path.exists(file_path):
            return jsonify({"error": "resource file does not exist"}), 400

        if file_path.endswith(".csv"):

            def csv_stream():
                with open(file_path) as f_o:
                    # Read 1000x1000 matrix size of
                    # data each stream
                    response_txt = f_o.read(1000000)
                    while response_txt != "":
                        yield response_txt
                        response_txt = f_o.read(1000000)

            return Response(
                csv_stream(),
                mimetype="text/csv",
                headers={
                    "Content-disposition": ("attachment;filename={}").format(
                        download_file_name
                    )
                },
            )
        else:
            return send_file(
                file_path,
                as_attachment=True,
                attachment_filename=download_file_name,
                mimetype="application/octet-stream",
            )

    except Exception:
        return jsonify({"error": "not able to read file"}), 500


@app.route("/consensus/<task_id>/files/<data_type>/csv")
def get_csv_by_consensus_taskid(task_id, data_type):
    """ """
    root_file_path = common.app_config["APP_DATA"]["DATA_FILES_ROOT"]
    download_file_name = "genotypes_{}.csv".format(str(time.time()))
    SQL_GET_PARENT_CONSENUS_SAMPLES = (
        "SELECT consensusSampleIndex, consensusSampleName FROM consensus_samples"
    )
    SQL_GET_CONSENSUS_VALUES = (
        "SELECT * FROM consensus_{data_type} "
        "WHERE consensusSampleIndex="
        "'{consensus_sample_index}'"
    )
    SQL_GET_REPLICATES_VALUES = "SELECT * FROM '{}_{}'"

    try:
        consensus_db_path = os.path.join(root_file_path, task_id, "consensus.db")

        if os.path.exists(consensus_db_path):
            consensus_db = sqlite.connect(consensus_db_path)
            parent_consensus_samples = consensus_db.execute(
                SQL_GET_PARENT_CONSENUS_SAMPLES.format(data_type=data_type)
            ).fetchall()

            def stream_data():
                row_count = 0
                for parent_samples in parent_consensus_samples:
                    genotypes_csv = ""
                    parent_sample_index = parent_samples[0]
                    parent_sample_name = parent_samples[1]
                    parent_genotypes_cursor = consensus_db.execute(
                        SQL_GET_CONSENSUS_VALUES.format(
                            data_type=data_type,
                            consensus_sample_index=parent_sample_index,
                        )
                    )
                    parent_genotypes = parent_genotypes_cursor.fetchone()
                    if row_count == 0:
                        header = [
                            description[0]
                            for description in parent_genotypes_cursor.description
                        ]
                        genotypes_csv += ",".join(header) + "\n"
                        genotypes_csv = genotypes_csv.replace("consensusSampleIndex", "consensusSampleName", 1)
                        row_count += 1
                    parent_genotypes_csv = ",".join(parent_genotypes) + "\n"
                    parent_genotypes_csv = parent_genotypes_csv.replace(parent_sample_index, parent_sample_name, 1)
                    genotypes_csv += parent_genotypes_csv
                    genotypes_fetch_cursor = consensus_db.execute(
                        SQL_GET_REPLICATES_VALUES.format(parent_sample_index, data_type)
                    )
                    replicate_genotypes = genotypes_fetch_cursor.fetchmany(1000)
                    while replicate_genotypes:
                        for genotypes_row in replicate_genotypes:
                            genotypes_row = [str(x) for x in genotypes_row]
                            genotypes_csv += ",".join(genotypes_row) + "\n"
                        row_count += len(replicate_genotypes)
                        yield genotypes_csv
                        replicate_genotypes = genotypes_fetch_cursor.fetchmany(1000)
                        genotypes_csv

            return Response(
                stream_data(),
                mimetype="text/csv",
                headers={
                    "Content-disposition": ("attachment;filename={}").format(
                        download_file_name
                    )
                },
            )
        else:
            return jsonify({"error": "unable to process request."}), 400

    except Exception as e:
        print(str(e))
        return jsonify({"error": "unable to process request."}), 500


@app.route("/consensus/<task_id>/genotypes/<parent_index>")
def get_data_by_consensus_parent(task_id, parent_index):
    result_lst = []
    root_file_path = common.app_config["APP_DATA"]["DATA_FILES_ROOT"]
    SQL_GET_PARENT_CONSENUS_NAME = (
        "SELECT consensusSampleName FROM consensus_samples WHERE consensusSampleIndex='{}'"
    )
    SQL_GET_PARENT_CONSENUS_GENOTYPES = (
        "SELECT * FROM consensus_genotypes WHERE consensusSampleIndex='{}'"
    )
    SQL_GET_PARENT_CONSENUS_MERGED = "SELECT * FROM '{}_genotypes' "

    try:
        consensus_db_path = os.path.join(root_file_path, task_id, "consensus.db")
        if os.path.exists(consensus_db_path):
            consensus_db = sqlite.connect(consensus_db_path)
            consensus_sample_cursor = consensus_db.execute(
                SQL_GET_PARENT_CONSENUS_NAME.format(parent_index)
            )
            parent_name = consensus_sample_cursor.fetchone()[0]
            parent_consensus_cursor = consensus_db.execute(
                SQL_GET_PARENT_CONSENUS_GENOTYPES.format(parent_index)
            )
            parent_consensus_genotypes = parent_consensus_cursor.fetchall()
            if len(parent_consensus_genotypes) > 0:
                header = [
                    description[0]
                    for description in parent_consensus_cursor.description
                ]
                result_value = {}
                for idx, value in enumerate(parent_consensus_genotypes[0]):
                    if header[idx] == "consensusSampleIndex":
                        result_value["sampleName"] = parent_name
                    else:
                        result_value[header[idx]] = value
                result_lst.append(result_value)

                replicate_genotypes_cursor = consensus_db.execute(
                    SQL_GET_PARENT_CONSENUS_MERGED.format(parent_index)
                )
                header = [
                    description[0]
                    for description in replicate_genotypes_cursor.description
                ]
                replicate_genotypes = replicate_genotypes_cursor.fetchall()
                for genotype_row in replicate_genotypes:
                    result_value = {}
                    for idx, value in enumerate(genotype_row):
                        if header[idx] == "replicateSampleName":
                            result_value["sampleName"] = value
                        else:
                            result_value[header[idx]] = value
                    result_lst.append(result_value)
                return jsonify(result=result_lst)
            else:
                return jsonify({"error": "no parent consensus tp process."}), 400
        else:
            return jsonify({"error": "unable to process request."}), 400
    except Exception as e:
        print(str(e))
        return jsonify({"error": "system error."}), 500


@app.route("/consensus/<task_id>/parent-genotypes", methods=["PATCH"])
def update_parent_consensus(task_id):
    root_file_path = common.app_config["APP_DATA"]["DATA_FILES_ROOT"]
    SQL_GET_PARENT_CONSENUS_SAMPLE_INDEX = (
        "SELECT consensusSampleIndex FROM consensus_samples WHERE consensusSampleName='{}'"
    )
    SQL_UPDATE_PARENT_CONSENUS_GENOTYPES = (
        "UPDATE consensus_genotypes SET '{}'='{}' WHERE consensusSampleIndex='{}'"
    )
    SQL_GET_PARENT_CONSENUS_GENOTYPES = (
        "SELECT * FROM consensus_genotypes WHERE consensusSampleIndex='{}'"
    )
    try:
        consensus_db_path = os.path.join(root_file_path, task_id, "consensus.db")
        request_body = request.get_json()
        try:
            update_args = UpdateConsensusSchema().load(request_body)
        except ValidationError as err:
            return {"errors": err.messages}, 400
        if os.path.exists(consensus_db_path):
            consensus_db = sqlite.connect(consensus_db_path)
            consensus_sample_cursor = consensus_db.execute(
                SQL_GET_PARENT_CONSENUS_SAMPLE_INDEX.format(update_args["parentSampleName"])
            )
            parent_index = consensus_sample_cursor.fetchone()[0]
            parent_consensus_cursor = consensus_db.execute(
                SQL_GET_PARENT_CONSENUS_GENOTYPES.format(parent_index)
            )
            parent_consensus_genotypes = parent_consensus_cursor.fetchall()
            if len(parent_consensus_genotypes) > 0:
                consensus_db.execute(
                    SQL_UPDATE_PARENT_CONSENUS_GENOTYPES.format(
                        update_args["markerName"],
                        update_args["genotypeValue"],
                        parent_index,
                    )
                )
                consensus_db.commit()
                header = [
                    description[0]
                    for description in parent_consensus_cursor.description
                ]
                result_value = {}
                parent_consensus_cursor = consensus_db.execute(
                    SQL_GET_PARENT_CONSENUS_GENOTYPES.format(parent_index)
                )
                parent_consensus_genotypes = parent_consensus_cursor.fetchall()
                for idx, value in enumerate(parent_consensus_genotypes[0]):
                    if header[idx] == "consensusSampleName":
                        result_value["sampleName"] = value
                    else:
                        result_value[header[idx]] = value
            else:
                return jsonify({"error": "no parent consensus to process."}), 400
            return jsonify(result_value)
        else:
            return jsonify({"error": "unable to process request."}), 400
    except Exception as e:
        print(str(e))
        return jsonify({"error": "system error."}), 500


@app.route("/files/<task_id>/<file_name>/preview")
def get_file_preview_by_taskid(task_id, file_name):

    root_file_path = common.app_config["APP_DATA"]["DATA_FILES_ROOT"]
    preview_file_name = file_name + ".preview"
    preview_header_file_name = preview_file_name + ".header"
    preview_file_path = os.path.join(root_file_path, task_id, preview_file_name)
    preview_header_file_path = os.path.join(
        root_file_path, task_id, preview_header_file_name
    )

    preview_data = {}

    if os.path.exists(preview_file_path):
        try:
            # TODO: Crude way of coverting to ordered dict to format it to json.
            # might need a refactot to O(n)
            preview_df = pd.read_csv(
                preview_file_path, sep="\t", dtype="str", comment="#", index_col=0
            )
            preview_json = preview_df.to_json(orient="index")
            preview_json_ordered_dict = json.loads(
                preview_json, object_pairs_hook=OrderedDict
            )
            result_lst = []
            for sample_name in preview_json_ordered_dict:
                preview_json_ordered_dict[sample_name]["sampleName"] = sample_name
                result_lst.append(preview_json_ordered_dict[sample_name])

            preview_data["result"] = result_lst

            if os.path.exists(preview_header_file_path):
                with open(preview_header_file_path) as preview_f_:
                    preview_data["header"] = json.load(preview_f_)

            return jsonify(preview_data), 200
        except:
            return jsonify({"error": "not able to get preview"}), 400
    else:
        return jsonify({"error": "preview file does not exist"}), 400
