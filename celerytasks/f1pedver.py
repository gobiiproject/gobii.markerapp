"""
Created on Jul 31, 2019

@author: JIgnacio
"""
import os

import math
import numpy as np
import pandas as pd
import copy
import sys
from subprocess import call
from subprocess import Popen
from collections import OrderedDict
import shutil

import common

class dataset:
    """
    class of a dataset composed of genotype and sample information
    """

    def __init__(
        self,
        genotype_data,
        sample_data,
        name=None,
        parents=None,
        samples=None,
        parent_columns=None,
        parent_search_column=None,
        sep="/",
        mis="N",
    ):
        self.genotype_data = genotype_data
        self.sample_data = sample_data
        self.nsamples = self.genotype_data.shape[0]
        self.nmarkers = self.genotype_data.shape[1]
        self.name = name
        self.parents = parents
        self.samples = samples
        self.parent_columns = parent_columns
        self.parent_search_column = parent_search_column
        self.index_of_parents = None
        self.sep = sep
        self.mis = mis
        self.sample_call_rates = []
        self.marker_call_rates = []
        self.__first_three_columns = [
            "germplasmName",
            "pedigree",
            "additionalInfo_pedigree",
            "germplasmType",
            "sampleName",
            "callSetName",
        ]

    def export_genotype_data_to_file(self, filename):
        missing_value = self.mis
        if self.sep:
            missing_value = "{mis}{sep}{mis}".format(mis=self.mis,
                                                     sep=self.sep)
        # Normalise missing values
        self.genotype_data.replace(missing_value,
                                   common.GENOTYPES_DATA_SPECS.MISSING_CHAR,
                                   inplace=True)
        self.genotype_data.to_csv(filename, index=True, na_rep='',
                                  sep="\t", mode="w", line_terminator="\n")

    def export_sample_data_to_file(self, filename, header=True):
        self.sample_data.to_csv(
            filename,
            index=True,
            na_rep="",
            sep="\t",
            mode="w",
            line_terminator="\n",
            header=header,
        )

    def export_to_file(self, filename):
        self.export_genotype_data_to_file(filename + ".gt.txt")
        self.export_sample_data_to_file(filename + ".sm.txt")

    def __get_column_with_dtype(self, column_name):
        if np.issubdtype(self.sample_data[column_name].dtype, np.number):
            return column_name + "_#NUM"
        return column_name + "_#CAT"

    def export_to_file_fj_format(self, filename):
        self.export_genotype_data_to_file(filename + ".gt.txt")
        sample_data_fj_columns = list(self.sample_data.columns)
        first_three_columns = []
        for column_name in self.__first_three_columns:
            if column_name in sample_data_fj_columns:
                sample_data_fj_columns.remove(column_name)
                first_three_columns.append(column_name)
        sample_data_fj_columns = first_three_columns + sample_data_fj_columns
        sample_data_fj_headers = [
            self.__get_column_with_dtype(column_name)
            for column_name in sample_data_fj_columns
        ]
        self.sample_data = self.sample_data[sample_data_fj_columns]
        self.export_sample_data_to_file(
            filename + ".sm.txt", header=sample_data_fj_headers
        )

    def set_name(self, name):
        self.name = name

    def set_parents(self, parent_columns=None):
        if parent_columns is not None:
            self.parent_columns = parent_columns
        elif self.parent_columns is None:
            raise TypeError(
                "Parent column has not been set,"
                "please specify parent_columns "
                "e.g. ['germplasm_par1','germplasm_par2']"
            )
        parents = []
        for column in self.parent_columns:
            parents = parents + self.sample_data[column].tolist()
        # Drop duplicates and nan
        parents = list(dict.fromkeys(parents))
        self.parents = set([x for x in parents if str(x) != "nan"])

    def set_index_of_parents(self, parent_search_column=None):
        if parent_search_column is None:
            if self.parent_search_column is not None:
                parent_search_column = self.parent_search_column
        self.parent_search_column = parent_search_column

        # sample indexes for indetified parents
        self.index_of_parents = []

        # keep track of how many replicated each parents have
        replicates_count = {}

        # keeps track of sample index by parent, so we can discard indexes with
        # no replicates
        sample_index_by_parent = {}

        # Count the replicates and save sample indexes for each parent
        for sample_idx, sample_data in self.sample_data.iterrows():
            if sample_data[parent_search_column] in self.parents:
                if (
                    sample_data[parent_search_column] in replicates_count
                    and sample_data[parent_search_column] in sample_index_by_parent
                ):
                    replicates_count[sample_data[parent_search_column]] += 1
                    sample_index_by_parent[sample_data[parent_search_column]].append(
                        sample_idx
                    )
                else:
                    replicates_count[sample_data[parent_search_column]] = 1
                    sample_index_by_parent[sample_data[parent_search_column]] = [
                        sample_idx
                    ]

        # if parent have replicate count more than one than add its index
        for parent in replicates_count:
            if replicates_count[parent] > 0:
                self.index_of_parents += sample_index_by_parent[parent]

    def get_parent_genotype_data(self):
        if self.parents is None:
            self.set_parents(self.parent_columns)
        if self.index_of_parents is None:
            self.set_index_of_parents()
        gd = self.genotype_data.loc[self.index_of_parents]
        return gd

    def get_parent_sample_data(self):
        if self.parents is None:
            self.set_parents(self.parent_columns)
        if self.index_of_parents is None:
            self.set_index_of_parents()
        sd = self.sample_data.loc[self.index_of_parents]
        return sd

    def concat(self, ds):
        self.genotype_data = self.genotype_data.append(ds.genotype_data)
        self.sample_data = self.sample_data.append(ds.sample_data)
        return self

    def get_parent_data(self):
        if self.parents is None:
            self.set_parents(self.parent_columns)
        if self.index_of_parents is None:
            self.set_index_of_parents()
        gd = self.genotype_data.loc[self.index_of_parents]
        sd = self.sample_data.loc[self.index_of_parents]
        new_ds = dataset(genotype_data=gd, sample_data=sd)
        new_ds.parent_columns = self.parent_columns
        new_ds.parent_search_column = self.parent_search_column
        new_ds.sep = self.sep
        new_ds.mis = self.mis
        return new_ds

    def filter_genotype_data(self, column, value):
        gd = self.genotype_data
        sd = self.sample_data
        idx = [x == value for x in sd[column]]
        return gd.loc[idx]

    def filter_sample_data(self, column, value):
        sd = self.sample_data
        idx = [x == value for x in sd[column]]
        return sd.loc[idx]

    def filter(self, idx=None, column=None, value=None):
        gd = self.genotype_data
        sd = self.sample_data
        if idx is None:
            if column is None or value is None:
                raise TypeError(
                    "Please specify indices, or column and value, to filter data with."
                )
            idx = [x == value for x in sd[column]]
        return dataset(gd.loc[idx], sd.loc[idx])

    def filter_for_parents_of_y(self, y, parent_search_column=None):
        parents = set(y.parents)
        index_of_parents = [
            idx
            for idx, x in self.sample_data.iterrows()
            if x[parent_search_column] in parents
        ]
        gd = self.genotype_data.loc[index_of_parents]
        sd = self.sample_data.loc[index_of_parents]
        return dataset(genotype_data=gd, sample_data=sd)

    def createFJProjectFile(
        self,
        file,
        name,
        separator="/",
        missing="N/N",
        qtlfile=None,
        mapfile=None,
        jarfile=None,
    ):
        sfile = file + ".sm.txt"
        gfile = file + ".gt.tmp"
        cmd = [
            "java",
            "-cp",
            jarfile,
            "jhi.flapjack.io.cmd.CreateProject",
            "-A",
            "-g",
            gfile,
            "-t",
            sfile,
            "-p",
            "output.flapjack",
            "-n",
            name,
            "-S",
            separator,
            "-M",
            missing,
        ]
        if qtlfile:
            cmd += ["-q", qtlfile]
        if mapfile:
            cmd += ["-m", mapfile]
        call(cmd)

    def createFJHeader(self, name, headerjar):
        sfile = name + ".sm.txt"
        gfile = name + ".gt.txt"
        cmd = ["java", "-jar", headerjar, sfile, gfile, name + ".gt.tmp"]
        p = Popen(cmd)
        p.wait()
        # call(cmd)

    def set_call_rates(self):
        self.sample_call_rates = []
        self.marker_call_rates = []

        shape = self.genotype_data.shape
        n_samples = float(shape[0])
        n_markers = float(shape[1])

        mis = self.mis + self.sep + self.mis

        for x, y in self.genotype_data.iteritems():
            if mis in set(y):
                self.marker_call_rates.append(
                    round(1.0 - (float(list(y).count(mis)) / n_samples), 3)
                )
            else:
                self.marker_call_rates.append(1.0)

        for x, y in self.genotype_data.iterrows():
            if mis in set(y):
                self.sample_call_rates.append(
                    round(1.0 - (float(list(y).count(mis)) / n_markers), 3)
                )
            else:
                self.sample_call_rates.append(1.0)

    def get_filter_index(self, sample_call_rate=None, marker_call_rate=None):
        if len(self.marker_call_rates) == 0 | len(self.sample_call_rates) == 0:
            self.set_call_rates()
        if marker_call_rate is not None:
            markers_to_keep = list(
                map(lambda x: x > marker_call_rate, self.marker_call_rates)
            )
        else:
            markers_to_keep = self.genotype_data.columns
        if sample_call_rate is not None:
            samples_to_keep = list(
                map(lambda x: x > sample_call_rate, self.sample_call_rates)
            )
        else:
            samples_to_keep = self.genotype_data.index
        return markers_to_keep, samples_to_keep

    # remove samples or markers below call rates
    def filter_genotype(self, sample_call_rate=None, marker_call_rate=None):
        markers_to_keep, samples_to_keep = self.get_filter_index(
            sample_call_rate, marker_call_rate
        )
        new_gd = self.genotype_data.loc[samples_to_keep, markers_to_keep]
        new_sd = self.sample_data.loc[self.genotype_data.index[samples_to_keep]]
        new_ds = dataset(
            new_gd,
            new_sd,
            parent_columns=self.parent_columns,
            parent_search_column=self.parent_search_column,
            sep=self.sep,
            mis=self.mis,
        )
        return new_ds


class splitter:
    """
    class for splitting datasets
    """

    def __init__(
        self,
        dataset,
        germplasm_name_column="germplasm_name",
        par1="germplasm_par1",
        par2="germplasm_par2",
        sep="/",
        mis="N",
    ):
        self.genotype_data = dataset.genotype_data
        self.sample_data = dataset.sample_data
        self.germplasm_name_column = germplasm_name_column
        self.par1 = par1
        self.par2 = par2
        self.sep = sep
        self.mis = mis
        self.group_column_names = None
        self.parents = None
        self.group_list = None
        self.group_idx = None

    def generate_split_dict(self, *cols):
        sample_data = self.sample_data
        queue = list(cols)
        idx = OrderedDict({"initdict": sample_data.index})
        while queue:
            col = queue.pop(0)
            tmp_idx = OrderedDict()
            out_idx = OrderedDict(
                map(
                    lambda x_y: (
                        x_y[0],
                        self.recursive_split(x_y[0], sample_data.loc[x_y[1]], col),
                    ),
                    idx.items(),
                )
            )
            for key, value in out_idx.items():
                tmp_idx.update(value)
            idx = tmp_idx
        self.group_list = list(map(lambda x_y: x_y[0], idx.items()))
        self.group_idx = idx

    def split(self, *cols):
        if cols:
            self.generate_split_dict(*cols)
        split_dataset = OrderedDict(
            map(
                lambda x_y: (
                    x_y[0],
                    dataset(
                        self.genotype_data.loc[x_y[1]], self.sample_data.loc[x_y[1]]
                    ),
                ),
                self.group_idx.items(),
            )
        )
        return split_dataset

    def recursive_split(self, parent_name, sample_data, col):
        groups = sample_data[col].unique()
        valid_groups = [x for x in groups if pd.notna(x)]
        na_groups = [x for x in groups if pd.isna(x)]

        idx = OrderedDict(
            map(
                lambda x: (
                    self.rename_group(parent_name, x),
                    sample_data.loc[pd.isna(sample_data[col])].index,
                ),
                na_groups,
            )
        )

        idx.update(
            OrderedDict(
                map(
                    lambda x: (
                        self.rename_group(parent_name, x),
                        sample_data.loc[sample_data[col] == x].index,
                    ),
                    valid_groups,
                )
            )
        )
        return idx

    def rename_group(self, parent_name, child_name):
        parent_name = str(parent_name)
        child_name = str(child_name)
        if parent_name == "initdict":
            new_name = child_name
        else:
            if parent_name == "nan":
                if child_name == "nan":
                    new_name = "nan"
                else:
                    new_name = "nan_" + child_name
            elif child_name == "nan":
                new_name = parent_name
            else:
                new_name = parent_name + "_" + child_name
        return new_name

    def get_parents(self):
        # Split sample file #
        group_list = self.sample_data[self.column_name1].drop_duplicates()
        mergedlist = []
        for index, item in group_list.iteritems():
            if isinstance(item, float):
                if math.isnan(item):
                    continue
            elif isinstance(item, str):
                if not item:
                    continue
            df = self.sample_data[self.sample_data[self.column_name1] == item]

            # store dnaruns of parents in a list
            par1 = list(set(filter(lambda x: str(x) != "nan", df[self.par1])))
            par2 = list(set(filter(lambda x: str(x) != "nan", df[self.par2])))
            lst1 = list(
                self.sample_data.loc[
                    self.sample_data[self.germplasm_name_column].isin(par1), :
                ].index
            )
            lst2 = list(
                self.sample_data.loc[
                    self.sample_data[self.germplasm_name_column].isin(par2), :
                ].index
            )
            mergedlist = mergedlist + lst1 + lst2
        self.parents = list(dict.fromkeys(mergedlist))

    def splitData(self):
        # Split sample file #
        group_list = self.sample_data[self.column_name1].drop_duplicates()
        for index, item in group_list.iteritems():
            if isinstance(item, float):
                if math.isnan(item):
                    continue
            elif isinstance(item, str):
                if not item:
                    continue
            df = self.sample_data[self.sample_data[self.column_name1] == item]

            # store dnaruns of parents in a list
            par1 = list(set(filter(lambda x: str(x) != "nan", df["germplasm_par1"])))
            par2 = list(set(filter(lambda x: str(x) != "nan", df["germplasm_par2"])))
            lst1 = list(
                self.sample_data.loc[
                    self.sample_data["germplasm_name"].isin(par1), :
                ].index
            )
            lst2 = list(
                self.sample_data.loc[
                    self.sample_data["germplasm_name"].isin(par2), :
                ].index
            )
            mergedlst = lst1 + lst2

            subgroup_list = df[self.column_name2].drop_duplicates()
            for idx, sub in subgroup_list.iteritems():
                if isinstance(sub, float):
                    if math.isnan(sub):
                        if not item in self.parents and mergedlst:
                            self.parents.update({item: mergedlst})
                        continue
                elif isinstance(sub, str):
                    if not sub:
                        continue

                subkey = item + "_" + sub
                if not subkey in self.parents and mergedlst:
                    self.parents.update({subkey: lst1 + lst2})

        # Split genotype file based on sample information #
        self.splitfile(
            self.genotype_data,
            self.sample_data,
            self.parents,
            group_list,
            subgroup_list,
        )

    def createProjectFile(
        self, samplefile, genofile, jarfile, separator, missing, qtlfile, mapfile
    ):
        sample_data = pd.read_table(samplefile, dtype="str")
        groups = sample_data.dnasample_sample_group.drop_duplicates()
        for index, key in groups.iteritems():
            if isinstance(key, float) and math.isnan(key):
                continue
            df = sample_data[sample_data.dnasample_sample_group == key]
            subgroup_list = df.dnasample_sample_group_cycle.drop_duplicates()
            for idx, sub in subgroup_list.iteritems():
                if isinstance(sub, float) and math.isnan(sub):
                    name = key
                elif isinstance(sub, str) and not sub:
                    name = key
                else:
                    name = key + "_" + sub
                name = str(name)
                sfile = samplefile + "_" + name + ".txt"
                gfile = genofile + "_" + name + ".txt.tmp"
                cmd = [
                    "java",
                    "-cp",
                    jarfile,
                    "jhi.flapjack.io.cmd.CreateProject",
                    "-A",
                    "-g",
                    gfile,
                    "-t",
                    sfile,
                    "-p",
                    genofile + ".flapjack",
                    "-n",
                    name,
                    "-S",
                    separator,
                    "-M",
                    missing,
                ]
                if qtlfile:
                    cmd += ["-q", qtlfile]
                if mapfile:
                    cmd += ["-m", mapfile]
                print(cmd)
                call(cmd)

    def createHeader(self, samplefile, genofile, headerjar):
        sample_data = pd.read_table(samplefile, dtype="str")
        groups = sample_data.dnasample_sample_group.drop_duplicates()
        for index, key in groups.iteritems():
            if isinstance(key, float) and math.isnan(key):
                continue
            df = sample_data[sample_data.dnasample_sample_group == key]
            subgroup_list = df.dnasample_sample_group_cycle.drop_duplicates()
            for idx, sub in subgroup_list.iteritems():
                if isinstance(sub, float) and math.isnan(sub):
                    name = key
                elif isinstance(sub, str) and not sub:
                    name = key
                else:
                    name = key + "_" + sub
                name = str(name)
                sfile = samplefile + "_" + name + ".txt"
                gfile = genofile + "_" + name + ".txt"
                cmd = ["java", "-jar", headerjar, sfile, gfile, gfile + ".tmp"]
                call(cmd)

    @staticmethod
    def batch_create_fj(
        path,
        split_datasets,
        separator="/",
        missing="N/N",
        qtlfile=None,
        mapfile=None,
        jarfile=None,
        header=True,
        will_clean_up=True,
    ):
        """
        Replaced the non optimal map methods being called multiple times
        by vg249
        """

        qfile = "qtl.txt.tmp"
        mfile = "map.txt.tmp"
        bfile = os.path.join(path, "batch.txt.tmp")
        fjp = os.path.join(path, "output.flapjack")

        batch_file = open(bfile, "w")

        batch_file.write("map\tgeno\tsample\tqtl\tname\n")

        if qtlfile is None:
            qfile = ""
        else:
            shutil.copyfile(qtlfile, path + qfile)

        if mapfile is None:
            mfile = ""
        else:
            shutil.copyfile(mapfile, os.path.join(path, mfile))

        for split_dataset_name, file_name in split_datasets.items():
            batch_file.write(
                splitter.get_ds_batch_line(
                    split_dataset_name, file_name, qfile, mfile, header
                )
            )

        batch_file.close()

        if os.path.exists(fjp):
            os.remove(fjp)

        cmd = [
            "java",
            "-cp",
            jarfile,
            "jhi.flapjack.io.cmd.CreateProjectBatch",
            "-b",
            bfile,
            "-A",
            "-p",
            fjp,
            "-S",
            separator,
            "-M",
            missing,
        ]

        call(cmd)

        if will_clean_up:
            splitter.clean_up(bfile)
            if qtlfile is not None:
                splitter.clean_up(path + qfile)
            if mapfile is not None:
                splitter.clean_up(path + mfile)
            for split_dataset_name, split_dataset in split_datasets.items():
                splitter.clean_up(path + split_dataset_name + ".sm.txt")
                splitter.clean_up(path + split_dataset_name + ".gt.txt")
                if header is not None:
                    splitter.clean_up(path + split_dataset_name + ".gt.tmp")

    def get_ds_batch_line(name, file_name, qtlfile, mapfile, header=True):
        sfile = file_name + ".sm.txt"
        if header is True:
            gfile = file_name + ".gt.tmp"
        else:
            gfile = file_name + ".gt.txt"
        line = (
            mapfile + "\t" + gfile + "\t" + sfile + "\t" + qtlfile + "\t" + name + "\n"
        )
        return line

    def create_fj_header(self, name, headerjar):
        sfile = name + ".sm.txt"
        gfile = name + ".gt.txt"
        cmd = ["java", "-jar", headerjar, sfile, gfile, name + ".gt.tmp"]
        return cmd

    def parallel_fj_header_creation(self, path, names, headerjar, limit=4):
        commands = map(
            lambda x_y: self.create_fj_header(path + x_y[0], headerjar), names.items()
        )
        groups = [(Popen(cmd) for cmd in commands)] * limit  # itertools' grouper recipe
        pass
        # for processes in izip_longest(*groups):  # run len(processes) == limit at a time
        #    for p in filter(None, processes):
        #        p.wait()

    def clean_up(filename):
        if os.path.exists(filename):
            os.remove(filename)


class consensus:
    """
    Class for consensus genotype calling
    """

    def __init__(
        self,
        sample_data,
        genotype_data,
        group_column="germplasm_name",
        sep="/",
        mis="N",
        threshold=0.5,
        consensus_db=None,
    ):
        """
        Constructor
        """
        if not isinstance(sample_data, pd.DataFrame):
            raise TypeError("sample_data must be a pandas dataframe")
        if not isinstance(genotype_data, pd.DataFrame):
            raise TypeError("genotype_data must be a pandas dataframe")
        self.sample_data = sample_data
        self.genotype_data = genotype_data
        self.group_column = group_column
        self.consensus_db = consensus_db
        self.sep = sep
        self.mis = mis
        self.threshold = threshold
        self.encoded_genotype = None
        self.cgenotype_data = None
        self.csample_data = None
        self.consensus_parents = []
        self.inconclusive_result = self.mis + self.sep + self.mis

    def get_consensus_data(self):
        return self.genotype_data, self.sample_data

    def get_consensus_winners(self, consensus_group_by_val):
        """Gets the consensus winner for each markers of given
        parent value's replicates.
        """
        consensus_winners_by_markers = {}
        sample_replicates = self.sample_data[
            self.sample_data[self.group_column] == consensus_group_by_val
        ]
        genotype_replicates = self.genotype_data.loc[
            sample_replicates.index.tolist(), :
        ]
        for marker in genotype_replicates:
            genotype_counts = genotype_replicates[marker].value_counts()
            uniform_het_index = []
            valid_genotypes_counts = {}
            total_valid_genotypes = 0
            for genotype_index in genotype_counts.index:
                if self.is_valid_consensus_genotype(genotype_index):
                    if genotype_index[0] > genotype_index[2]:
                        genotype = genotype_index[2] + self.sep + genotype_index[0]
                    else:
                        genotype = genotype_index
                    if genotype in valid_genotypes_counts:
                        valid_genotypes_counts[genotype] += genotype_counts[genotype_index]
                    else:
                        valid_genotypes_counts[genotype] = genotype_counts[genotype_index]
                    total_valid_genotypes += genotype_counts[genotype_index] 
                else:
                    continue
            
            if total_valid_genotypes == 0:
                consensus_winners_by_markers[marker] = self.inconclusive_result
                continue

            genotype_freqs = {}
            for _genotype in valid_genotypes_counts:
                genotype_freqs[_genotype] = valid_genotypes_counts[_genotype]/total_valid_genotypes

            max_freq = max(genotype_freqs.values())
            if max_freq < self.threshold:
                consensus_winners_by_markers[marker] = self.inconclusive_result
                continue
            consensus_winners = []
            for _genotype in genotype_freqs:
                if genotype_freqs[_genotype] == max_freq:
                    consensus_winners.append(_genotype)

            num_of_contenders = len(consensus_winners)

            if num_of_contenders == 1:
                if self.is_valid_consensus_genotype(consensus_winners[0]):
                    consensus_winners_by_markers[marker] = consensus_winners[0]
                else:
                    consensus_winners_by_markers[marker] = self.inconclusive_result
            else:
                consensus_winners_by_markers[marker] = self.get_homozygous_winner(
                    consensus_winners
                )
        return consensus_winners_by_markers

    def get_homozygous_winner(self, possible_winners):
        num_of_homozygous = 0
        homozygous_result = self.inconclusive_result
        for candidate in possible_winners:
            if self.is_valid_consensus_genotype(candidate):
                if candidate[0] == candidate[2]:
                    if num_of_homozygous > 0:
                        return self.inconclusive_result
                    homozygous_result = candidate
                    num_of_homozygous += 1
        return homozygous_result

    def is_valid_consensus_genotype(self, genotype):
        """Checks wether the given genotype value is valid
        example: for given sep: /, G/G; A/T are valid values.
        if there is not separator, GG, AT are valid values
        anything other than that are invalid for consensus calling.

        params:
            genotype: value to checked

        returns: True/False
        """
        valid_alleles = ["A", "C", "G", "T", "+", "-"]
        if not self.sep:
            return False
        genotype_split = genotype.split(self.sep)
        if len(genotype_split) == 2:
            genotypes_alleles = set(genotype_split)
            if genotypes_alleles.issubset(valid_alleles):
                return True
        return False

    def make_consensus(self):
        try:

            group_list = self.sample_data[self.group_column].drop_duplicates()

            marker_names = list(self.genotype_data)
            consensus_parent_genotypes = pd.DataFrame(columns=marker_names)
            consensus_parent_samples = pd.DataFrame(columns=list(self.sample_data))
            
            num_index = 1
            for index, item in group_list.iteritems():
                if isinstance(item, float):
                    if math.isnan(item):
                        continue
                elif isinstance(item, str):
                    if not item:
                        continue

                consensus_parent_name = f"{item}*"
                consensus_parent_index = f"consensus_parent_{num_index}"
                genotype_table_name = f"{consensus_parent_index}_genotypes"
                samples_table_name = f"{consensus_parent_index}_samples"

                _replicate_samples = self.sample_data[
                    self.sample_data[self.group_column] == item
                ]

                consensus_winners_by_markers = self.get_consensus_winners(item)

                group_column_value = item
                
                self.genotype_data.loc[_replicate_samples.index.tolist(), :].fillna(
                    value=""
                ).to_sql(
                    genotype_table_name,
                    con=self.consensus_db,
                    index=True,
                    index_label="replicateSampleName",
                    if_exists="append",
                )

                _replicate_samples = _replicate_samples.fillna(value="")
                _replicate_samples.to_sql(
                    samples_table_name,
                    con=self.consensus_db,
                    index=True,
                    index_label="replicateSampleName",
                    if_exists="append",
                )

                consensus_parent_genotypes.loc[
                    consensus_parent_index
                ] = consensus_winners_by_markers

                tmp_df_sample = pd.DataFrame(
                    {self.group_column: group_column_value},
                    index=[consensus_parent_index],
                )
                tmp_df_sample["consensusSampleName"] = consensus_parent_name
                consensus_parent_samples = pd.concat(
                    [consensus_parent_samples, tmp_df_sample], axis=0, sort=False
                )

                self.consensus_parents.append({
                    "name": consensus_parent_name,
                    "index": consensus_parent_index
                })

                num_index += 1

            consensus_parent_genotypes.fillna(value="").to_sql(
                "consensus_genotypes",
                con=self.consensus_db,
                index=True,
                index_label="consensusSampleIndex",
                if_exists="append",
            )

            consensus_parent_samples.fillna(value="").to_sql(
                "consensus_samples",
                con=self.consensus_db,
                index=True,
                index_label="consensusSampleIndex",
                if_exists="append",
            )

            self.cgenotype_data = consensus_parent_genotypes
            self.csample_data = consensus_parent_samples
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
