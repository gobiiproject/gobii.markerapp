

file_download_url = "/files/{task_id}/{file_name}"

preview_file_url = file_download_url + "/preview"

consensus_genotypes_url = "/consensus/{task_id}/files/genotypes/csv"

consensus_samples_url = "/consensus/{task_id}/files/samples/csv"

consensus_parent_genotypes_url = (
    "/consensus/{task_id}/genotypes/{parent_name}")
