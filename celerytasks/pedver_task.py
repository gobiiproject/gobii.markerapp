from celery.exceptions import Ignore


class PedverTask(object):

    def __init__(self, task):
        """ Constructor.
        """
        self.task = task
        self.task_id = task.request.id
        self.completed_tasks = []

    def _update_data_reader_progress(self, progress):
        """ Updates the task object with progress of datareader.

        Args:
            progress: DataReaderProgress object returned by datareader.
        """

        self.task.update_state(
            state=progress.state,
            meta={
                "message": progress.message,
                "completed_tasks": self.completed_tasks
            })

        if progress.state.endswith("FAILED"):
            raise Ignore()

        elif progress.state.endswith("COMPLETED"):
            self.completed_tasks.append({
                "task": progress.state.replace("-COMPLETED", ""),
                "status": progress.message
            })

    def _update_task_status(self, task_state, message):
        """ Updates task object with state and message.

        Args:
            task_state (str): {task_type}-(INPROGRESS | COMPLETED | FAILED).
                example: LOAD-PREVIEW-COMPLETED, FILTER-GENOTYPES-COMPLETED.
            message (str): task status message
        """

        if task_state.endswith("COMPLETED"):
            self.completed_tasks.append({
                "task": task_state,
                "status": message
            })

        self.task.update_state(state=task_state, meta={
            'message': message,
            'completed_tasks': self.completed_tasks
        })

    def _get_genotypes_callsets_dfs(self, data_reader, *args):
        """ Process datareader yields

        Args:
            data_reader (DataReader): data reader object
            variantset_id (str): id of the dataset to be read.

        Returns:
            tuple of (genotypes_df, callsets_df)
                genotypes_df: dataframe with genotypes result.
                callsets_df: dataframe with callsets(dnaruns) result.
        """

        # read genotypes data
        genotypes_df = self._get_genotypes_df(data_reader, *args)

        # read callsets data
        callsets_df = self._get_callsets_df(data_reader, *args)

        return genotypes_df, callsets_df

    def _get_genotypes_df(self, data_reader, *args):

        genotypes_df = None
        for progress in data_reader.get_genotypes(*args):
            self._update_data_reader_progress(progress)
            genotypes_df = progress.result
        return genotypes_df

    def _get_callsets_df(self, data_reader, *args):

        callsets_df = None

        for progress in data_reader.get_callsets(*args):
            self._update_data_reader_progress(progress)
            callsets_df = progress.result

        return callsets_df

    def _get_markerpositions(self, data_reader, *args):

        markerpositions_df = None

        for progress in data_reader.get_markerpositions(*args):
            self._update_data_reader_progress(progress)
            markerpositions_df = progress.result

        return markerpositions_df
