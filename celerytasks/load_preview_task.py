import os
import sys

import common

from .celery import app
from celery.exceptions import Ignore

import celerytasks.settings as settings
import celerytasks.utils as utils
import celerytasks.url_template as url_template

from .data_reader import PreviewDataReader

from celerytasks.pedver_task import PedverTask


class LoadPreviewTask(PedverTask):

    def __init__(self, task):
        """ Constructor.

        Args:
            task: Celery task bound to the instance.
        """

        super().__init__(task)

        self.__get_file_preview_url = url_template.preview_file_url.format(
            task_id=self.task_id,
            file_name="genotypes")

        self.__get_preview_file_path = settings.get_preview_file_path(
            self.task_id)

    def run(self,
            brapi_url,
            auth_token,
            variantset_ids,
            mapset_id):
        """Runs the task to generate preview file.

        Reads slice of 10x10 (10 markers, 10 dnaruns) of data from all given
        datasets and merge them by marker ids using an outer join to create
        one single preview dataset.
        Creates a header json object for the same and saves them in the file.

        Args:
            brapi_url: brapi server url
            auth_token: authorization token for brapi server
            variantset_ids: ids of datasets to preview
            mapset_id: id of the mapset to map the variant to
              the marker positions.
        """

        genotypes_df = None
        markerpositions_df = None

        missing_value = common.GENOTYPES_DATA_SPECS.MISSING_VALUE

        variant_ids = []

        if mapset_id is not None:
            mapset_ids = [mapset_id]
        else:
            mapset_ids = []

        try:

            data_reader = PreviewDataReader(brapi_base_url=brapi_url,
                                            auth_token=auth_token)

            self._update_task_status("LOAD-PREVIEW-INPROGRESS",
                                     "rading data")

            genotypes_result = self._get_genotypes_df(data_reader,
                                                      variantset_ids)
            genotypes_df = genotypes_result["df"]
            variant_ids = list(genotypes_result["variant_ids"])

            if genotypes_df is not None:

                genotypes_df.fillna(missing_value, inplace=True)
                utils.df_to_csv(genotypes_df, self.__get_preview_file_path)

                if len(mapset_ids) > 0:
                    markerpositions_df = self._get_markerpositions(data_reader,
                                                                   variant_ids,
                                                                   mapset_ids)
                # convert markerpositions dataframe to dictionary object
                markerpositions_dict = None
                if markerpositions_df is not None:
                    markerpositions_dict = markerpositions_df.to_dict("index")

                self.__create_preview_headers(genotypes_df.columns,
                                              markerpositions_dict)

                completion_msg = {
                    "filePreviewUrl": self.__get_file_preview_url
                }
                self.completed_tasks.append({
                    "task": "LOAD-PREVIEW",
                    "status": completion_msg
                })
                self._update_task_status("LOAD-PREVIEW-COMPLETED",
                                         completion_msg)
            else:
                self._update_task_status("LOAD-PREVIEW-FAILED",
                                         "unable to read data")

        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            self._update_task_status("LOAD-PREVIEW-FAILED",
                                     "system error")
            raise Ignore()
        raise Ignore()

    def __create_preview_headers(self,
                                 header_columns,
                                 markerpositions_dict):
        """ Creates header json object for given header columns
        and marker positions.

        Args:
            header_columns: list of column names. marker names.
            markerpositions_dict: dictionary object with meta data to map
              marker names to position, linkage group and genome maps.
        """
        headers = {}

        for column_name in header_columns:
            headers[column_name] = {
                "variantName": column_name
            }
            if markerpositions_dict is not None:
                if column_name in markerpositions_dict:

                    headers[column_name]["linkageGroupName"] = (
                        markerpositions_dict[
                            column_name].get("linkageGroupName", ""))

                    headers[column_name]["position"] = (
                        str(markerpositions_dict[
                            column_name].get("position", ""))
                    )
                else:
                    headers[column_name]["linkageGroupName"] = ""
                    headers[column_name]["position"] = ""

        if len(headers) > 0:
            header_file_path = (
                settings.get_preview_header_file_path(self.task_id))
            utils.dict_to_json(headers, header_file_path)


@app.task(name="tasks.load_preview", bind=True)
def load_preview(self, **kwargs):
    LoadPreviewTask(self).run(**kwargs)
