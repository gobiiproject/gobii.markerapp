#!/usr/bin/python

import os
import common

import sqlite3 as sqlite

path_join = lambda *paths: os.path.join(*paths)

mk_dir = lambda dir_path: os.makedirs(dir_path)

CACHE_DIR = os.path.join(common.ROOT_FILE_PATH, "cached_variantsets")

# Creates cache dir in root file path.
if os.path.exists(common.ROOT_FILE_PATH):
    os.makedirs(CACHE_DIR, exist_ok=True)
else:
    raise Exception("Root file path is not a valid path")


GENOME_MAPS_FILE_NAME_TEMPLATE = "{mapname}.map"

# Name of the flapkjack project file
OUTPUT_PROJECT_FILE_NAME = "output.flapjack"

CACHE_VARIANTSETS_PATH_TEMPLATE = path_join(CACHE_DIR,
                                            "{brapi_resource_hash}",
                                            "{variantset_id}")

CACHE_GENOTYPES_FILE_NAME = "genotypes.csv"

CACHE_CALLSETS_FILE_NAME = "callsets.csv"

# Genotypes preview file names
GENOTYPES_PREVIEW_FILE_NAME = "genotypes.preview"
GENOTYPES_PREVIEW_HEADER_FILE_NAME = "genotypes.preview.header"

# Filtered intermediate file names
FILTERED_GENOTYPES_FILE_NAME = "genotypes.filtered.csv"
FILTERED_CALLSETS_FILE_NAME = "callsets.filtered.csv"
FILTERED_OUTPUT_PREVIEW_FILE_NAME = "genotypes.filtered.csv.preview"

ETAG_FILE_NAME="etag"

def get_preview_file_path(task_id):
    return path_join(common.get_task_dir(task_id),
                     GENOTYPES_PREVIEW_FILE_NAME)


def get_preview_header_file_path(task_id):
    return path_join(common.get_task_dir(task_id),
                     GENOTYPES_PREVIEW_HEADER_FILE_NAME)


def get_variantset_cache_path(brapi_resource_hash, variantset_id):
    """ Returns the cache path for given brapi resource and variantset_id"""

    variantset_cahce_path = CACHE_VARIANTSETS_PATH_TEMPLATE.format(
        brapi_resource_hash=brapi_resource_hash,
        variantset_id=variantset_id)

    if not os.path.exists(variantset_cahce_path):
        mk_dir(variantset_cahce_path)
    return variantset_cahce_path

def get_etag_file_path(brapi_resource_hash, variantset_id):
    """ Returns the etag file path for given brapi resource and
    variantset_id"""
    return path_join(get_variantset_cache_path(brapi_resource_hash,
                                               variantset_id),
                     ETAG_FILE_NAME)


def get_genotypes_cache_file_path(brapi_resource_hash, variantset_id):
    """" Retruns file path of genotypes cache file path"""

    return path_join(get_variantset_cache_path(brapi_resource_hash,
                                               variantset_id),
                     CACHE_GENOTYPES_FILE_NAME)


def get_callsets_cache_file_path(brapi_resource_hash, variantset_id):
    """" Retruns file path of callsets cache file path"""

    return path_join(get_variantset_cache_path(brapi_resource_hash,
                                               variantset_id),
                     CACHE_CALLSETS_FILE_NAME)


def get_maps_file_path(task_id, map_id):
    return path_join(common.get_task_dir(task_id),
                     GENOME_MAPS_FILE_NAME_TEMPLATE.format(mapname=map_id))


def filtered_genotypes_file_path(task_id):
    return path_join(common.get_task_dir(task_id),
                     FILTERED_GENOTYPES_FILE_NAME)


def filtered_callsets_file_path(task_id):
    return path_join(common.get_task_dir(task_id),
                     FILTERED_CALLSETS_FILE_NAME)


def filterd_output_preview_path(task_id):
    return path_join(common.get_task_dir(task_id),
                     FILTERED_OUTPUT_PREVIEW_FILE_NAME)


def get_consensus_db(task_id):
    return path_join(common.get_task_dir(task_id),
                     "consensus.db")


def get_consensus_db_conn(task_id):
    try:
        return sqlite.connect(get_consensus_db(task_id))
    except Exception:
        raise ValueError("System Error: Consensus DB initializtion")


SQL_GET_CONSENSUS_GENOTYPES = "SELECT * FROM consensus_{}"
SQL_GET_CONSENSUS_SAMPLES = "SELECT * FROM consensus_samples"
SQL_GET_PARENT_CONSENUS_SAMPLES = ("SELECT consensusSampleName "
                                   "FROM consensus_samples")
SQL_GET_CONSENSUS_GENOTYPES_BY_NAME = (SQL_GET_CONSENSUS_GENOTYPES +
                                       " WHERE consensusSampleName="
                                       "'{consensus_sample_name}'")
SQL_GET_CONSENSUS_SAMPLES_BY_NAME = (SQL_GET_CONSENSUS_SAMPLES +
                                     " WHERE consensusSampleName="
                                     "'{consensus_sample_name}'")
SQL_GET_REPLICATES_GENOTYPES = "SELECT * FROM '{}genotypes'"
SQL_GET_REPLICATES_SAMPLES = "SELECT * FROM '{}samples'"


OUTPUT_PROJECT_FILE_NAME = "output.flapjack"

GENOME_MAP_FILE_COLUMNS = ["variantName",
                           "linkageGroupName",
                           "position"]
