import os
import sys
import copy

import common

from celery.exceptions import Ignore

from .celery import app
import celerytasks.settings as settings
import celerytasks.url_template as url_template

from celerytasks.pedver_task import PedverTask

from .data_reader import FilterDataReader

from .f1pedver import dataset
from .f1pedver import consensus


class ConsensusTask(PedverTask):
    def __init__(self, task):
        """Constructor

        Args:
            task: Celery.Task object bound to the instance
        """

        super().__init__(task)

        self.__file_download_url = url_template.consensus_genotypes_url.format(
            task_id=self.task_id
        )

        self.__sample_metadata_download_url = url_template.consensus_samples_url.format(
            task_id=self.task_id
        )

        self.__consensus_db_conn = settings.get_consensus_db_conn(self.task_id)

    def run(
        self,
        brapi_url=None,
        auth_token=None,
        variantset_ids=[],
        filter_task_id=None,
        consensus_threshold=0,
    ):
        """Runs the consensus task for the given paramters.

        Reads the data using the given data paramters and performs consensus
        calling on the same.
        If filter task id is provided, it reads the result genotypes and
        callsets of given filter task and use them as input data.

        Args:
            brapi_url: brapi server url
            auth_token: authorization token for brapi server
            variantset_ids: ids of datasets to preview
            filter_task_id: id of the filter task.
            consensus_threshold: (int) threshold value for consensus.
        """

        genotypes_df = None
        callsets_df = None

        parent_columns = []
        parent_search_column = None
        consensus_column = "germplasmName"

        missing = common.GENOTYPES_DATA_SPECS.MISSING_VALUE

        try:

            self._update_task_status(
                "CONSENSUS-CALLING-INPROGRESS", "reading genotype data"
            )

            data_reader = FilterDataReader(
                brapi_base_url=brapi_url,
                auth_token=auth_token,
                filter_task_id=filter_task_id,
            )

            genotypes_df = self._get_genotypes_df(data_reader, variantset_ids)

            callsets_df = self._get_callsets_df(data_reader, variantset_ids)

            if genotypes_df is None or callsets_df is None:
                raise ValueError("unable to read data")

            genotypes_df.fillna(missing, inplace=True)
            callsets_df = callsets_df.fillna("").applymap(str)

            parent_columns = ConsensusTask.get_parent_columns(
                callsets_df.columns.values
            )

            if len(parent_columns) > 0:
                parent_search_column = consensus_column
            else:
                raise ValueError("no parent data to do consensus calling")

            self._update_task_status(
                "CONSENSUS-CALLING-INPROGRESS", "consensus calling in progress"
            )

            dataset_ = dataset(
                genotype_data=genotypes_df,
                sample_data=callsets_df,
                parent_columns=parent_columns,
                parent_search_column=parent_search_column,
                sep=common.GENOTYPES_DATA_SPECS.SEPERATOR_CHAR,
                mis=common.GENOTYPES_DATA_SPECS.MISSING_CHAR,
            )

            if parent_columns is not None and len(parent_columns) > 0:
                parent_dataset_ = dataset_.get_parent_data()

                if (
                    dataset_.index_of_parents is not None
                    and len(dataset_.index_of_parents) > 1
                ):

                    consensus_ = consensus(
                        sample_data=parent_dataset_.sample_data,
                        genotype_data=parent_dataset_.genotype_data,
                        group_column=consensus_column,
                        sep=common.GENOTYPES_DATA_SPECS.SEPERATOR_CHAR,
                        mis=common.GENOTYPES_DATA_SPECS.MISSING_CHAR,
                        threshold=consensus_threshold,
                        consensus_db=self.__consensus_db_conn,
                    )

                else:
                    raise ValueError("no replicate samples available ")
            else:
                raise ValueError("no parent data to do consensus calling")

            consensus_.make_consensus()

            self._update_task_status(
                "CONSENSUS-CALLING-INPROGRESS", "saving consensus called dataset"
            )

            consensus_parents = []
            for consensus_parent in consensus_.consensus_parents:
                consensus_parents.append(
                    {
                        "parentName": consensus_parent["name"],
                        "genotypesUrl": (
                            url_template.consensus_parent_genotypes_url.format(
                                task_id=self.task_id, parent_name=consensus_parent["index"]
                            )
                        ),
                    }
                )

            completed_tasks_msg = {
                "fileDownloadUrl": self.__file_download_url,
                "sampleMetaDataUrl": self.__sample_metadata_download_url,
            }

            self.completed_tasks.append(
                {"task": "CONSENSUS-CALLING", "status": completed_tasks_msg}
            )

            completion_msg = copy.copy(completed_tasks_msg)
            completion_msg["consensusParents"] = consensus_parents

            self._update_task_status("CONSENSUS-CALLING-COMPLETED", completion_msg)

        except ValueError as e:
            self._update_task_status("CONSENSUS-CALLING-FAILED", str(e))
            raise Ignore()
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            self._update_task_status("CONSENSUS-CALLING-FAILED", "system error")
            raise Ignore()
        raise Ignore()

    @staticmethod
    def get_parent_columns(columns):
        """Checks whether parent columns are found in given list.

        The column names are renamed from additionalInfo_par1,
        additionalInfo_par2 to par1 and par2.
        To maintian compatibility to old data, check old column names too.
        """
        parent_columns = []
        if "additionalInfo_par1" in columns and "additionalInfo_par2" in columns:
            parent_columns = ["additionalInfo_par1", "additionalInfo_par2"]
        elif "par1" in columns and "par2" in columns:
            parent_columns = ["par1", "par2"]
        return parent_columns


@app.task(name="tasks.consensus_calling", bind=True)
def consensus_calling(self, **kwargs):
    ConsensusTask(self).run(**kwargs)
