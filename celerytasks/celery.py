import common
from celery.schedules import crontab


app = common.make_celery()

app.conf['imports'] = ["celerytasks.load_preview_task",
                       "celerytasks.filter_genotypes_task",
                       "celerytasks.consensus_task",
                       "celerytasks.split_genotypes_task",
                       "celerytasks.clean_up_beat_task"]

app.conf.beat_schedule = {
    # Executes every day at midnight.
    'run-every-day-midnight': {
        'task': 'tasks.daily_clean_up',
        'schedule': crontab(minute=0, hour=0),
        "args": ()
    },
}

if __name__ == '__main__':
    app.start()
