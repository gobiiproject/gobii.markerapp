#!/usr/bin/python

import os

from celerytasks.data_reader import DataReader
from celerytasks import settings


class FilterDataReader(DataReader):
    """ Reads filted data for given fiter task id. If the filter data not
    available, gets the original input data either from cache to api server."""

    def __init__(self,
                 brapi_base_url=None,
                 auth_token=None,
                 filter_task_id=None):
        DataReader.__init__(self, brapi_base_url, auth_token)
        self.filter_task_id = filter_task_id

    def get_genotypes(self, variantset_ids):

        filter_file_path = settings.filtered_genotypes_file_path(
            self.filter_task_id)

        if (self.filter_task_id is not None
                and os.path.isfile(filter_file_path)):
            return self._yield_file_data_reader_progress(filter_file_path,
                                                         "genotypes")
        else:
            return super().get_genotypes(variantset_ids)

    def get_callsets(self, variantset_ids):

        filter_file_path = settings.filtered_callsets_file_path(
            self.filter_task_id)

        if (self.filter_task_id is not None
                and os.path.isfile(filter_file_path)):
            return self._yield_file_data_reader_progress(filter_file_path,
                                                         "callsets")
        else:
            return super().get_callsets(variantset_ids)

    def get_markerpositions(self):
        pass
