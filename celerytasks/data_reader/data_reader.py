#!/usr/bin/python

import os
from http import HTTPStatus

from celerytasks import utils
from celerytasks import settings

from .brapi_downloader_factory import BrapiDownloaderFactory
from .data_reader_progress import DataReaderProgress
from .brapi import Brapi


class DataReader:
    """ Reads genotypes and callsets data from brapi api or cache folder for
    the parent task. It also updates the tasks.state and status objects.
    """

    __downlaoder_factory = BrapiDownloaderFactory()

    def __init__(self, brapi_base_url=None, auth_token=None):
        self.brapi_base_url = brapi_base_url
        self.auth_token = auth_token
        self._brapi_resource_hash = utils.md5_hash(brapi_base_url)
        self._brapi = Brapi(brapi_base_url=brapi_base_url,
                            auth_token=auth_token)

    def _yield_downloader_progress(self, downloader_yields, file_path=None):
        for progress in downloader_yields:
            if progress.state.endswith("COMPLETED") and file_path is not None:
                progress.result.fillna("", inplace=True)
                if file_path is not None:
                    utils.df_to_csv(progress.result, file_path)
            yield progress

    def _yield_file_data_reader_progress(self, file_path, data_type):

        df = utils.get_file_as_df(file_path)
        df.fillna("", inplace=True)
        data_reader_progress = DataReaderProgress(
            "READ-%s-COMPLETED" % data_type.upper(),
            "read %s completed" % data_type,
            df)

        yield data_reader_progress

    def get_variantset(self, variantset_id):
        etag_file = settings.get_etag_file_path(self._brapi_resource_hash,
                                                variantset_id)
        if_none_match = None
        if os.path.isfile(etag_file):
            with open(etag_file) as etag_f_:
                if_none_match = etag_f_.read()

        VARIANTSET_BY_ID_URL = "brapi/v2/variantsets/{variantset_id}"
        variantset_url = self._brapi.get_brapi_url(
            self.brapi_base_url,
            VARIANTSET_BY_ID_URL.format(variantset_id=variantset_id))

        headers = self._brapi.get_default_header()

        if if_none_match is not None:
            headers["If-None-Match"] = if_none_match

        brapi_result = self._brapi.brapi_get(variantset_url,
                                             headers)

        if "etag" in brapi_result and brapi_result["etag"] is not None:
            with open(etag_file, "w") as etag_f_:
                etag_f_.write(brapi_result["etag"])

        return brapi_result

    def get_genotypes(self, variantset_ids=[]):
        """ Gets genotypes data for given variant set id"""

        result_df = None

        def download_data(variantset_id, cache_file_path):
            downloader = self.__downlaoder_factory.get_downloader(
                self.brapi_base_url,
                self.auth_token,
                "genotypes")

            downloader_yields = (
                downloader.get_genotypes_by_variantsetid(variantset_id))

            return self._yield_downloader_progress(
                downloader_yields,
                file_path=cache_file_path)

        for variantset_id in variantset_ids:

            brapi_vs_result = self.get_variantset(variantset_id)

            genotypes_cache_file_path = (
                settings.get_genotypes_cache_file_path(
                    self._brapi_resource_hash,
                    variantset_id)
            )

            if brapi_vs_result["status_code"] == HTTPStatus.NOT_MODIFIED:
                if os.path.isfile(genotypes_cache_file_path):
                    progresses = self._yield_file_data_reader_progress(
                        genotypes_cache_file_path,
                        "genotypes")
                else:
                    progresses = download_data(variantset_id,
                                               genotypes_cache_file_path)

            elif brapi_vs_result["status_code"] == HTTPStatus.OK:
                progresses = download_data(variantset_id,
                                           genotypes_cache_file_path)
            else:
                continue
            for progress in progresses:
                if progress.state.endswith("COMPLETED"):
                    result_df = self._merge_genotype_dfs(result_df,
                                                         progress.result,
                                                         variantset_id)
                progress.result = result_df
                yield progress

    def _merge_genotype_dfs(self, result_df, df_to_merge, variantset_id):
        df_to_merge = df_to_merge.set_index(
            df_to_merge.index.astype(str) + "_"
            + str(variantset_id))
        if result_df is None:
            result_df = df_to_merge
        else:
            result_df = result_df.T.join(
                df_to_merge.T,
                how="outer")
            result_df = result_df.T
        return result_df

    def get_callsets(self, variantset_ids=[]):
        """ Gets callsets data for given variant set id"""

        result_df = None

        def download_data(variantset_id, cache_file_path):
            downloader = self.__downlaoder_factory.get_downloader(
                self.brapi_base_url,
                self.auth_token,
                "callsets")

            downloader_yields = (
                downloader.get_callsets_by_varianset_id(variantset_id))

            return self._yield_downloader_progress(
                downloader_yields)

        for variantset_id in variantset_ids:

            brapi_vs_result = self.get_variantset(variantset_id)

            if (brapi_vs_result["status_code"] == HTTPStatus.OK or
                    brapi_vs_result["status_code"] == HTTPStatus.NOT_MODIFIED):

                progresses = download_data(variantset_id, None)
            else:
                continue

            for progress in progresses:
                if progress.state.endswith("COMPLETED"):
                    progress.result = progress.result.set_index(
                        progress.result.index.astype(str) + "_"
                        + str(variantset_id))
                    if result_df is None:
                        result_df = progress.result
                    else:
                        result_df = result_df.append(progress.result,
                                                     sort=False)
                        progress.result = result_df
                else:
                    progress.result_df = result_df
                yield progress

    def get_markerpositions(self, variantset_ids, mapset_id):

        result_df = None
        progresses = None

        for variantset_id in variantset_ids:

            downloader = self.__downlaoder_factory.get_downloader(
                self.brapi_base_url, self.auth_token,
                "markerpositions")
            downloader_yields = downloader.get_markerpositions(
                variantSetDbId=variantset_id, mapDbId=mapset_id)

            progresses = self._yield_downloader_progress(downloader_yields)

            for progress in progresses:
                if progress.state.endswith("COMPLETED"):
                    if result_df is None:
                        result_df = progress.result
                    else:
                        result_df = result_df.append(
                            progress.result, sort=False).drop_duplicates()
                        progress.result = result_df
                else:
                    progress.result = result_df
                yield progress
