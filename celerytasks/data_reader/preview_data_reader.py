#!/usr/bin/python

import sys

from celerytasks import utils

from celerytasks.data_reader import DataReader
from celerytasks.data_reader import BrapiSearch
from celerytasks.data_reader import BRAPI_GLOBAL_ERROR_RESPONSE

from .brapi_downloader_factory import BrapiDownloaderFactory

from celerytasks.data_reader import DataReaderProgress


class PreviewDataReader(DataReader):
    """ Read 10 x 10 genotypes matrix for preview. """

    __downlaoder_factory = BrapiDownloaderFactory()

    __preview_matrix_size = 10

    def __init__(self, brapi_base_url=None, auth_token=None):
        DataReader.__init__(self, brapi_base_url, auth_token)

    def __get_downloader(self, downloader_type):
        return BrapiDownloaderFactory().get_downloader(self.brapi_base_url,
                                                       self.auth_token,
                                                       downloader_type)

    def get_genotypes(self, variantset_ids):

        result_df = None

        variant_ids_all = []
        callset_ids_all = []

        try:

            brapi_search = BrapiSearch(brapi_base_url=self.brapi_base_url,
                                       auth_token=self.auth_token)

            callsets_downloader = self.__get_downloader("callsets")

            variants_downloader = self.__get_downloader("variants")

            genotypes_downloader = self.__get_downloader("genotypes")

            for variantset_id in variantset_ids:

                callset_ids = []
                variant_ids = []

                for progress in (
                    callsets_downloader.get_callsets_by_varianset_id(
                        variantset_id,
                        pageSize=self.__preview_matrix_size,
                        last_page=0)):
                    if progress.state.endswith("COMPLETED"):
                        callset_ids = progress.result["callSetDbId"].tolist()

                for progress in (
                    variants_downloader.get_variants_by_variantset_id(
                        variantset_id,
                        pageSize=self.__preview_matrix_size, last_page=0)):
                    if progress.state.endswith("COMPLETED"):
                        variant_ids = progress.result["variantDbId"].tolist()

                if len(callset_ids) > 0 and len(variantset_ids) > 0:

                    genotypes_search_query = {
                        "variantSetDbIds": [variantset_id],
                        "callSetDbIds": callset_ids,
                        "variantDbIds": variant_ids
                    }

                    genotypes_search_result = (
                        brapi_search.submit_genotypes_search(
                            genotypes_search_query))

                    genotypes_search_query_id = (
                        genotypes_search_result["searchResultDbId"])

                    for progress in (
                        genotypes_downloader.get_genotypes_by_search_query(
                            genotypes_search_query_id)):
                        if progress.state.endswith("COMPLETED"):
                            result_df = self._merge_genotype_dfs(
                                result_df, progress.result, variantset_id)
                            brapi_downloader_progress = DataReaderProgress(
                                "GENOTYPES-READER-INPROGRESS",
                                "merging genotypes results")

                    variant_ids_all += variant_ids
                    callset_ids_all += callset_ids

            brapi_downloader_progress = DataReaderProgress(
                "GENOTYPES-READER-COMPLETED",
                "reading genotypes completed",
                {
                    "df": result_df,
                    "callset_ids": set(callset_ids_all),
                    "variant_ids": set(variant_ids_all)
                })

            yield brapi_downloader_progress

        except Exception:
            e = sys.exc_info()[0]
            utils.log("unknown exception: {}".format(e))
            brapi_downloader_progress = DataReaderProgress(
                "LOAD-PREVIEW-FAILED", BRAPI_GLOBAL_ERROR_RESPONSE)
            yield brapi_downloader_progress

    def get_callsets(self):
        pass

    def get_markerpositions(self, variant_ids, mapset_ids):

        markerpositions_df = None

        try:

            brapi_search = BrapiSearch(brapi_base_url=self.brapi_base_url,
                                       auth_token=self.auth_token)

            markerpositions_downloader = self.__get_downloader(
                "markerpositions")

            search_query = {
                "variantDbIds": variant_ids,
                "mapDbIds": mapset_ids
            }

            search_result = (
                brapi_search.submit_markerpositions_search(search_query))

            search_query_id = search_result["searchResultDbId"]

            for progress in (markerpositions_downloader
                             .get_markerpositions_by_search_query(
                                 search_query_id)):

                if progress.state.endswith("COMPLETED"):

                    markerpositions_df = progress.result

            brapi_downloader_progress = DataReaderProgress(
                "MARKERPOSITIONS-READER-COMPLETED",
                "reading markerpositions completed",
                markerpositions_df)

            yield brapi_downloader_progress

        except Exception:
            e = sys.exc_info()[0]
            utils.log("unknown exception: {}".format(e))
            brapi_downloader_progress = DataReaderProgress(
                "LOAD-PREVIEW-FAILED", BRAPI_GLOBAL_ERROR_RESPONSE)
            yield brapi_downloader_progress
