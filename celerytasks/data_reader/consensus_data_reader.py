
import pandas as pd

from celerytasks.data_reader import DataReaderProgress

from celerytasks import settings

import queries


class ConsensusDataReader:

    def __init__(self, task_id):
        self.consensus_task_id = task_id
        if task_id is None:
            raise ValueError("No consensus task id to do "
                             "consensus data reader")
        self.__db_conn = settings.get_consensus_db_conn(task_id)

    def __get_merged_data(self, data_type):
        """ Get data for given data_type"""

        df_merged = None

        try:

            parent_consensus_samples = self.__db_conn.execute(
                queries.get_parent_consenus_samples).fetchall()
            for parent_samples in parent_consensus_samples:

                parent_sample_index = parent_samples[0]

                # Read and merge parent consensus.
                df = pd.read_sql(
                    queries.get_consensus_by_index(parent_sample_index, data_type),
                    con=self.__db_conn,
                    index_col=queries.index_col)

                if df_merged is None:
                    df_merged = df
                else:
                    df_merged = df_merged.append(df)

                # Read and merge hybrid samples.
                df_merged = df_merged.append(pd.read_sql(
                    queries.get_replicates_by_index(parent_sample_index, data_type),
                    con=self.__db_conn,
                    index_col=queries.index_col))

        except Exception as e:
            print(str(e))
            raise ValueError(
                "system error: not able to read consensus database")

        return df_merged

    def get_genotypes(self):
        return self.__get_merged_data("genotypes")

    def get_callsets(self):
        return self.__get_merged_data("samples")

    def get_markerpositions(self):
        pass

    def get_consensus_genotypes(self):
        try:
            consensus_genotypes_df = pd.read_sql(
                queries.get_consensus_by_datatype("genotypes"),
                con=self.__db_conn,
                index_col=queries.index_col)
            return consensus_genotypes_df
        except Exception as e:
            print(str(e))
            raise ValueError(
                "system error: not able to read consensus database")

    def get_consensus_callsets(self):
        try:
            consensus_callsets_df = pd.read_sql(
                queries.get_consensus_by_datatype("samples"),
                con=self.__db_conn,
                index_col=queries.index_col)
            return consensus_callsets_df
        except Exception as e:
            print(str(e))
            raise ValueError(
                "system error: not able to read consensus database")
