#!/usr/bin/python

import sys

import requests
from http import HTTPStatus

from urllib.parse import urljoin

from celerytasks import utils

BRAPI_GLOBAL_ERROR_RESPONSE = {
    "error": "unable to read brapi data"
}


class Brapi(object):
    """Read data from Gobii GDM API.
    """
    def __init__(self, **kwargs):
        self.brapi_base_url = kwargs["brapi_base_url"]
        self.auth_token = kwargs["auth_token"]

    def get_default_header(self):
        header = {}
        if self.auth_token is not None:
            header["X-Auth-Token"] = self.auth_token
        return header

    def get_brapi_url(self, base_url="", relative_url=""):
        URL_PATH_SEP = "/"
        base_url = self.brapi_base_url.strip(URL_PATH_SEP) + URL_PATH_SEP
        relative_url = relative_url.strip(URL_PATH_SEP)
        return urljoin(base_url, relative_url)

    def get_q_params(self, q_params, q_params_default):
        """ Returns a request query parameter
        """
        if q_params is None:
            q_params = {}
        invalid_q_params = list(set(q_params) - set(q_params_default))
        if(len(invalid_q_params) > 0):
            invalid_q_str = ",".join(invalid_q_params)
            raise ValueError("Invalid arguments: {}".format(invalid_q_str))
        for q_key in q_params_default:
            if q_key not in q_params and q_params_default[q_key] is not None:
                q_params[q_key] = q_params_default[q_key]
        return q_params

    def brapi_get(self, url, headers=None, query_parameters=None):
        """
        Returns a dictionary with either "success" or "error" as key.
        if "success",value would be the reponse value as dictionary
        if "error",value would be error message
        """
        try:
            response = requests.get(url, headers=headers,
                                    params=query_parameters)

            return self.__parse_brapi_response(response, HTTPStatus.OK)
        except Exception:
            e = sys.exc_info()[0]
            utils.log("unknown exception: {}".format(e))
            return BRAPI_GLOBAL_ERROR_RESPONSE

    def brapi_post(self, url, headers=None, request_data=None):
        """
        Returns a dictionary with either "success" or "error" as key.
        if "success",value would be the reponse value as dictionary
        if "error",value would be error message
        """
        try:
            response = requests.post(url, headers=headers, json=request_data)

            return self.__parse_brapi_response(response, HTTPStatus.CREATED)
        except Exception:
            e = sys.exc_info()[0]
            utils.log("unknown exception: {}".format(e))
            return BRAPI_GLOBAL_ERROR_RESPONSE

    def __parse_brapi_response(self, response, success_code):

        return_value = {
            "status_code": response.status_code
        }

        if response.status_code == success_code:
            brapi_response = response.json()
            if "result" in brapi_response:
                return_value.update({
                    "success": brapi_response["result"],
                    "metadata": brapi_response["metadata"],
                })
                if "ETag" in response.headers:
                    return_value["etag"] = response.headers["ETag"]
            else:
                return_value.update({
                    "error": "BrAPI response has no result object"
                })
        elif response.status_code == HTTPStatus.NOT_MODIFIED:
            pass
        elif response.status_code == HTTPStatus.UNAUTHORIZED:
            return_value.update({"error": "Authentication Failed."})
        else:
            return_value.update({"error": "api error."})

        return return_value
