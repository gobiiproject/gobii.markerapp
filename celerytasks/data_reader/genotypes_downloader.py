
import sys

import pandas as pd

from celerytasks import utils

from celerytasks.data_reader import Brapi
from celerytasks.data_reader import BRAPI_GLOBAL_ERROR_RESPONSE
from celerytasks.data_reader import DataReaderProgress


class GenotypesDownloader(Brapi):
    """ Module to download genoytpes from a brapi resource.
    """

    BRAPI_CALLS_BY_VARIANTSETID_URL = (
        "brapi/v2/variantsets/{variantset_id}/calls")

    BRAPI_CALLS_BY_SEARCHID_URL = (
        "brapi/v2/search/calls/{search_query_id}")

    GENOTYPE_CALLS_QUERY_PARAMS = {
        "callSetDbId": None,
        "variantDbId": None,
        "variantSetDbId": None,
        "pageToken": None,
        "pageSize": 1000000
    }

    def __init__(self, **kwargs):
        Brapi.__init__(self, **kwargs)

    def get_genotypes_by_variantsetid(self, variantset_id, **kwargs):

        data_url = self.get_brapi_url(
                self.brapi_base_url,
                self.BRAPI_CALLS_BY_VARIANTSETID_URL).format(
                    variantset_id=variantset_id)
        headers = self.get_default_header()
        q_params = self.get_q_params(
            kwargs,
            self.GENOTYPE_CALLS_QUERY_PARAMS)

        return self.__yield_brapi_result(data_url, headers, q_params)

    def get_genotypes_by_search_query(self, search_query_id, **kwargs):

        data_url = self.get_brapi_url(
                self.brapi_base_url,
                self.BRAPI_CALLS_BY_SEARCHID_URL).format(
                    search_query_id=search_query_id)

        headers = self.get_default_header()

        q_params = self.get_q_params(
            kwargs,
            self.GENOTYPE_CALLS_QUERY_PARAMS)

        return self.__yield_brapi_result(data_url, headers, q_params)

    def __yield_brapi_result(self, data_url, headers, q_params):
        """ Read json data as dataframe.
        """

        first_page = True
        page_token = None
        result_data_length = 0
        total_gennotypes = 0

        error_msg = None

        df_dict = {}

        try:
            while first_page or result_data_length == q_params["pageSize"]:

                if not first_page:
                    q_params["pageToken"] = page_token
                else:
                    first_page = False

                result_data_length = 0

                response = self.brapi_get(
                    data_url, headers=headers,
                    query_parameters=q_params)

                if "error" in response:
                    error_msg = {"error": response["error"]}
                    break
                elif "success" in response:

                    result = response["success"]

                    for genotype in result["data"]:
                        if genotype["callSetName"] not in df_dict:
                            df_dict[genotype["callSetName"]] = {}
                        if ("genotype" in genotype
                                and len(genotype["genotype"]["values"]) > 0):
                            df_dict[genotype["callSetName"]][
                                    genotype["variantName"]] = (
                                genotype["genotype"]["values"][0])
                        else:
                            df_dict[genotype["callSetName"]][
                                    genotype["variantName"]] = ("N/N")

                    result_data_length = len(result["data"])
                    total_gennotypes += result_data_length

                    metadata = response["metadata"]

                    if ("pagination" in metadata
                            and "nextPageToken" in metadata["pagination"]):
                        page_token = metadata["pagination"]["nextPageToken"]

                    brapi_downloader_progress = DataReaderProgress(
                        "DOWNLOADING-GENOTYPES-INPROGRESS",
                        {"total_genotypes": total_gennotypes})

                    yield brapi_downloader_progress
                else:
                    error_msg = BRAPI_GLOBAL_ERROR_RESPONSE
                    break
            if error_msg is not None:
                brapi_downloader_progress = DataReaderProgress(
                    "DOWNLOADING-GENOTYPES-FAILED", error_msg)
                yield brapi_downloader_progress
            else:
                brapi_downloader_progress = DataReaderProgress(
                    "DOWNLOADING-GENOTYPES-COMPLETED",
                    {"total_genotypes": total_gennotypes},
                    pd.DataFrame.from_dict(df_dict, orient='index'))
                yield brapi_downloader_progress
        except Exception:
            e = sys.exc_info()[0]
            utils.log("unknown exception: {}".format(e))
            brapi_downloader_progress = DataReaderProgress(
                    "DOWNLOADING-GENOTYPES-FAILED",
                    BRAPI_GLOBAL_ERROR_RESPONSE)
            yield brapi_downloader_progress
        pass
