
import sys

import pandas as pd

from celerytasks import utils

from celerytasks.data_reader import Brapi
from celerytasks.data_reader import BRAPI_GLOBAL_ERROR_RESPONSE
from celerytasks.data_reader import DataReaderProgress


class VariantsDownloader(Brapi):
    """ Module to download callsets from a brapi resource.
    """

    BRAPI_VARIANTS_BY_VARIANTSETID_URL = (
        "brapi/v2/variantsets/{variantset_id}/variants")

    BRAPI_VARIANTS_BY_GENOTYPES_SEARCHID_URL = (
        "brapi/v2/search/calls/{search_query_id}/variants")

    VARIANTS_QUERY_PARAMS = {
            "variantDbId": None,
            "variantName": None,
            "variantSetDbId": None,
            "pageToken": None,
            "pageSize": 0,
    }

    def __init__(self, **kwargs):
        Brapi.__init__(self, **kwargs)

    def get_variants_by_variantset_id(self, variantset_id, **kwargs):

        last_page = kwargs.pop("last_page", None)
        data_url = self.get_brapi_url(
            self.brapi_base_url,
            self.BRAPI_VARIANTS_BY_VARIANTSETID_URL.format(
                variantset_id=variantset_id))
        headers = self.get_default_header()

        q_params = self.get_q_params(
            kwargs,
            self.VARIANTS_QUERY_PARAMS)

        return self.__yield_brapi_result(data_url, headers,
                                         q_params, last_page=last_page)

    def __yield_brapi_result(self, data_url, headers,
                             q_params, last_page=None):
        """ Downloads variants.
        returns:
            Dataframe with variants
        """
        first_page = True
        page_token = None
        result_data_length = 0
        total_variants = 0
        page = 0

        error_msg = None

        df_dict = {}

        try:
            while first_page or result_data_length == q_params["pageSize"]:

                if not first_page:
                    q_params["pageToken"] = page_token
                    page += 1
                    if last_page is not None and page >= last_page:
                        break
                else:
                    first_page = False

                result_data_length = 0

                response = self.brapi_get(
                    data_url, headers=headers,
                    query_parameters=q_params)

                if "error" in response:
                    error_msg = {"error": response["error"]}
                    break
                elif "success" in response:

                    result = response["success"]

                    for variant in result["data"]:
                        if ("variantNames" in variant
                                and len(variant["variantNames"]) > 0):
                            variant_name = variant["variantNames"][0]
                        else:
                            continue
                        if variant_name not in df_dict:
                            df_dict[variant_name] = {}
                        for variant_prop in variant:
                            df_dict[variant_name][variant_prop] = (
                                variant[variant_prop])

                    result_data_length = len(result["data"])
                    total_variants += result_data_length

                    metadata = response["metadata"]

                    if ("pagination" in metadata
                            and "nextPageToken" in metadata["pagination"]):
                        page_token = metadata["pagination"]["nextPageToken"]

                    brapi_downloader_progress = DataReaderProgress(
                        "DOWNLOADING-VARIANTS-INPROGRESS",
                        {"total_variants": total_variants})

                    yield brapi_downloader_progress
                else:
                    error_msg = BRAPI_GLOBAL_ERROR_RESPONSE
                    break
            if error_msg is not None:
                brapi_downloader_progress = DataReaderProgress(
                    "DOWNLOADING-VARIANTS-FAILED", error_msg)
                yield brapi_downloader_progress
            else:
                brapi_downloader_progress = DataReaderProgress(
                    "DOWNLOADING-VARIANTS-COMPLETED",
                    {"total_variants": total_variants},
                    pd.DataFrame.from_dict(df_dict, orient='index'))
                yield brapi_downloader_progress
        except Exception:
            e = sys.exc_info()[0]
            utils.log("unknown exception: {}".format(e))
            brapi_downloader_progress = DataReaderProgress(
                    "DOWNLOADING-VARIANTS-FAILED",
                    BRAPI_GLOBAL_ERROR_RESPONSE)
            yield brapi_downloader_progress
