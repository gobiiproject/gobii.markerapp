
from .genotypes_downloader import GenotypesDownloader
from .callsets_downloader import CallsetsDownloader
from .variants_downloader import VariantsDownloader
from .markerpositions_downloader import MarkerPositionsDownloader


class BrapiDownloaderFactory:
    """ Factory class for downloader.
    """

    __downloaders = {
        "genotypes": GenotypesDownloader,
        "callsets": CallsetsDownloader,
        "variants": VariantsDownloader,
        "markerpositions": MarkerPositionsDownloader
    }

    def get_downloader(self, brapi_base_url, auth_token, downloader_type):
        """ Factory method that returns respective brapi downloader
        for given type of data.
        """
        return self.__downloaders[downloader_type](
            brapi_base_url=brapi_base_url,
            auth_token=auth_token)
