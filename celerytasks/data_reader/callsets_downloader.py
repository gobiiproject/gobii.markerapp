
import sys

import pandas as pd

from celerytasks import utils

from celerytasks.data_reader import Brapi
from celerytasks.data_reader import BRAPI_GLOBAL_ERROR_RESPONSE
from celerytasks.data_reader import DataReaderProgress


class CallsetsDownloader(Brapi):
    """ Module to download callsets from a brapi resource.
    """

    BRAPI_CALLSETS_BY_VARIANTSETID_URL = (
        "brapi/v2/variantsets/{variantset_id}/callsets")

    BRAPI_CALLSETS_BY_SEARCHID_URL = (
        "brapi/v2/search/callsets/{search_query_id}")

    BRAPI_CALLSETS_BY_GENOTYPES_SEARCHID_URL = (
        "brapi/v2/search/calls/{search_query_id}/callsets")

    CALLSETS_QUERY_PARAMS = {
            "callSetDbId": None,
            "callSetName": None,
            "variantSetDbId": None,
            "sampleDbId": None,
            "germplasmDbId": None,
            "page": 0,
            "pageSize": 1000
    }

    def __init__(self, **kwargs):
        Brapi.__init__(self, **kwargs)

    def get_callsets_by_varianset_id(self, variantset_id, **kwargs):

        last_page = kwargs.pop("last_page", None)

        data_url = self.get_brapi_url(
            self.brapi_base_url,
            self.BRAPI_CALLSETS_BY_VARIANTSETID_URL.format(
                variantset_id=variantset_id))
        headers = self.get_default_header()
        q_params = self.get_q_params(
            kwargs,
            self.CALLSETS_QUERY_PARAMS)

        return self.__yield_brapi_result(data_url, headers,
                                         q_params, last_page=last_page)

    def get_callsets_by_search_query(self, search_query_id, **kwargs):
        data_url = self.get_brapi_url(
            self.brapi_base_url,
            self.BRAPI_CALLSETS_BY_SEARCHID_URL.format(
                search_query_id=search_query_id))
        headers = self.get_default_header()
        q_params = self.get_q_params(
            kwargs,
            self.CALLSETS_QUERY_PARAMS)

        return self.__yield_brapi_result(data_url, headers, q_params)

    def __yield_brapi_result(self, data_url, headers,
                             q_params, last_page=None):
        """ Download Callsets from brapi data source
        returns:
            DataFrame with downloaded dnaruns
        """
        first_page = True
        result_data_length = 0
        total_callsets = 0

        error_msg = None

        df_dict = {}

        try:

            while first_page or result_data_length == q_params["pageSize"]:

                if not first_page:
                    q_params["page"] += 1
                    if last_page is not None and q_params["page"] >= last_page:
                        break
                else:
                    first_page = False

                response = self.brapi_get(
                    data_url, headers=headers,
                    query_parameters=q_params)

                result_data_length = 0

                if "error" in response:
                    error_msg = {
                        "error": response["error"]
                    }
                    break
                elif "success" in response:

                    result = response["success"]

                    for callset in result["data"]:

                        if callset["callSetName"] not in df_dict:
                            df_dict[callset["callSetName"]] = {}

                        for callsetProp in callset:
                            self.__add_prop_to_df_dict(
                                df_dict, callset, callsetProp)

                    result_data_length = len(result["data"])

                    total_callsets += result_data_length

                    brapi_downloader_progress = DataReaderProgress(
                            "DOWNLOADING-CALLSETS-INPROGRESS",
                            {"total_callsets": total_callsets})
                    yield brapi_downloader_progress
                else:
                    error_msg = BRAPI_GLOBAL_ERROR_RESPONSE
                    break
            if error_msg is not None:
                brapi_downloader_progress = DataReaderProgress(
                    "DOWNLOADING-CALLSETS-FAILED", error_msg)
                yield brapi_downloader_progress
            else:
                brapi_downloader_progress = DataReaderProgress(
                    "DOWNLOADING-CALLSETS-COMPLETED",
                    {"total_callsets": total_callsets},
                    pd.DataFrame.from_dict(df_dict, orient='index'))
                yield brapi_downloader_progress
        except Exception:
            e = sys.exc_info()[0]
            utils.log("unknown exception: {}".format(e))
            brapi_downloader_progress = DataReaderProgress(
                    "DOWNLOADING-CALLSETS-FAILED",
                    BRAPI_GLOBAL_ERROR_RESPONSE)
            yield brapi_downloader_progress

    def __add_prop_to_df_dict(self, df_dict, callset, callsetProp):
        if (callsetProp != "callsetName" and callsetProp != "variantSetIds"):
            if callsetProp == 'additionalInfo':
                for additionalProp in callset[callsetProp]:
                    df_dict[callset["callSetName"]][additionalProp] = (
                        callset[callsetProp][additionalProp])
            else:
                df_dict[callset["callSetName"]][callsetProp] = (
                    callset[callsetProp])
