
import sys

import pandas as pd

from celerytasks import utils

from celerytasks.data_reader import Brapi
from celerytasks.data_reader import BRAPI_GLOBAL_ERROR_RESPONSE
from celerytasks.data_reader import DataReaderProgress


class MarkerPositionsDownloader(Brapi):
    """ Module to download callsets from a brapi resource.
    """

    BRAPI_MARKER_POSITIONS_URL = "brapi/v2/markerpositions"

    BRAPI_MARKER_POSITIONS_SEARCH_URL = (
        "brapi/v2/search/markerpositions/{search_query_id}")

    MARKER_POSITIONS_QUERY_PARAMS = {
        "mapDbId": None,
        "variantSetDbId": None,
        "page": 0,
        "pageSize": 1000
    }

    def __init__(self, **kwargs):
        Brapi.__init__(self, **kwargs)

    def get_markerpositions(self, **kwargs):

        data_url = self.get_brapi_url(
            self.brapi_base_url,
            self.BRAPI_MARKER_POSITIONS_URL)
        headers = self.get_default_header()
        q_params = self.get_q_params(
            kwargs,
            self.MARKER_POSITIONS_QUERY_PARAMS)

        return self.__yield_brapi_result(data_url, headers, q_params)

    def get_markerpositions_by_search_query(self, search_query_id, **kwargs):

        data_url = self.get_brapi_url(
            self.brapi_base_url,
            self.BRAPI_MARKER_POSITIONS_SEARCH_URL.format(
                search_query_id=search_query_id))

        headers = self.get_default_header()

        q_params = self.get_q_params(
            kwargs,
            self.MARKER_POSITIONS_QUERY_PARAMS)

        return self.__yield_brapi_result(data_url, headers, q_params)

    def __yield_brapi_result(self, data_url, headers, q_params):
        """ Downloads marker position for given mapDbId.
        returns:
            Dataframe with marker positions
        """
        first_page = True
        result_data_length = 0
        total_marker_positions = 0

        error_msg = None

        df_dict = {}

        try:

            while first_page or result_data_length == q_params["pageSize"]:

                if not first_page:
                    q_params["page"] += 1
                else:
                    first_page = False

                response = self.brapi_get(
                    data_url, headers=headers,
                    query_parameters=q_params)

                result_data_length = 0

                if "error" in response:
                    error_msg = {
                        "error": response["error"]
                    }
                    break
                elif "success" in response:

                    result = response["success"]

                    for marker_position in result["data"]:

                        if marker_position["variantName"] not in df_dict:
                            df_dict[marker_position["variantName"]] = {}

                        for marker_position_field in marker_position:
                            df_dict[marker_position[
                                "variantName"]][marker_position_field] = (
                                    marker_position[marker_position_field])

                    result_data_length = len(result["data"])
                    total_marker_positions += result_data_length

                    brapi_downloader_progress = DataReaderProgress(
                        "DOWNLOADING-MARKER_POSITIONS-INPROGRESS",
                        {"total_marker_positions": total_marker_positions})
                    yield brapi_downloader_progress
                else:
                    error_msg = BRAPI_GLOBAL_ERROR_RESPONSE
                    break
            if error_msg is not None:
                brapi_downloader_progress = DataReaderProgress(
                    "DOWNLOADING-MARKER_POSITIONS-FAILED", error_msg)
                yield brapi_downloader_progress
            else:
                brapi_downloader_progress = DataReaderProgress(
                    "DOWNLOADING-MARKER_POSITIONS-COMPLETED",
                    {"total_marker_positions": total_marker_positions},
                    pd.DataFrame.from_dict(df_dict, orient='index'))
                yield brapi_downloader_progress
        except Exception as e:
            print(str(e))
            e = sys.exc_info()[0]
            utils.log("unknown exception: {}".format(e))
            brapi_downloader_progress = DataReaderProgress(
                "DOWNLOADING-MARKER_POSITIONS-FAILED",
                BRAPI_GLOBAL_ERROR_RESPONSE)
            yield brapi_downloader_progress
