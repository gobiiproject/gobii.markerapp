
import sys

from celerytasks import utils
from celerytasks.data_reader import Brapi
from .brapi import BRAPI_GLOBAL_ERROR_RESPONSE


class BrapiSearch(Brapi):

    BRAPI_SEARCH_CALLS_URL = ("brapi/v2/search/calls")

    BRAPI_SEARCH_MARKERPOSITIONS_URL = ("brapi/v2/search/markerpositions")

    def __init__(self, **kwargs):
        Brapi.__init__(self, **kwargs)

    def submit_genotypes_search(self, search_query=None):
        search_url = self.get_brapi_url(
            self.brapi_base_url, self.BRAPI_SEARCH_CALLS_URL)

        return self.__submit_search(search_url, search_query)

    def submit_markerpositions_search(self, search_query=None):
        search_url = self.get_brapi_url(
            self.brapi_base_url, self.BRAPI_SEARCH_MARKERPOSITIONS_URL)

        return self.__submit_search(search_url, search_query)

    def __submit_search(self, search_url=None, search_query=None):

        try:

            headers = self.get_default_header()

            response = self.brapi_post(
                search_url, headers=headers,
                request_data=search_query)

            if "error" in response:
                raise ValueError(response["error"])
            elif "success" in response:
                return response["success"]
            else:
                raise ValueError("unknown expeption")

        except Exception:
            e = sys.exc_info()[0]
            utils.log("unknown exception: {}".format(e))
            return BRAPI_GLOBAL_ERROR_RESPONSE
