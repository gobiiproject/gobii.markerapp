#!/usr/bin/python


from celery.exceptions import Ignore

import os
import sys

import common

import celerytasks.settings as settings
import celerytasks.utils as utils
import celerytasks.url_template as url_template

from .data_reader import DataReader
from .data_reader import FilterDataReader
from .data_reader import ConsensusDataReader

from .f1pedver import dataset
from .f1pedver import splitter

from .celery import app

from celerytasks.pedver_task import PedverTask
from celerytasks.consensus_task import ConsensusTask


class SplitGenotypesTask(PedverTask):

    CONSENSUS_COLUMN = "germplasmName"

    def __init__(self, task):
        """Constructor

        Args:
            task: Celery.Task object bound to the instance
        """

        super().__init__(task)

    def run(self,
            brapi_url=None,
            auth_token=None,
            variantset_ids=[],
            filter_task_id=None,
            consensus_task_id=None,
            split_columns=None,
            mapset_id=None,
            project_file_num_parents=-1):

        project_datasets = {}
        split_statistics = {}
        completion_msg = {}

        try:

            self._update_task_status("SPLIT-GENOTYPES-INPROGRESS",
                                     "reading genotype data")

            if split_columns is None or len(split_columns) == 0:
                project_datasets = (
                    self.__get_dataset_without_split(consensus_task_id,
                                                     filter_task_id,
                                                     variantset_ids,
                                                     brapi_url,
                                                     auth_token))
            else:
                project_datasets, split_statistics = (
                    self.__get_datasets_with_split(split_columns,
                                                   consensus_task_id,
                                                   filter_task_id,
                                                   variantset_ids,
                                                   brapi_url,
                                                   auth_token,
                                                   project_file_num_parents))

            markerpositions_file_path = None

            if mapset_id is not None:
                data_reader = DataReader(brapi_base_url=brapi_url,
                                         auth_token=auth_token)
                markerpositions_file_path = self.__create_markerpositions_file(
                    data_reader,
                    variantset_ids,
                    mapset_id)

            if len(project_datasets) > 0:
                project_file_download_url = (
                    self.__create_project_file(project_datasets,
                                               markerpositions_file_path))

                completion_msg["fileDownloadUrl"] = project_file_download_url

            completion_msg["statistics"] = split_statistics

            self._update_task_status("SPLIT-GENOTYPES-COMPLETED",
                                     completion_msg)

        except ValueError as e:
            self._update_task_status("SPLIT-GENOTYPES-FAILED", str(e))
            raise Ignore()
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            self._update_task_status("SPLIT-GENOTYPES-FAILED", "system error")
            raise Ignore()
        raise Ignore()

    def __create_markerpositions_file(self,
                                      data_reader,
                                      variantset_ids,
                                      mapset_id):
        """ Read the marker positions for given mapset id in given datasets.

        Args:
            data_reader: DataReader for reading marker positions.
            variantset_ids: list of dataset ids
            mapset_id: genome mapset id
        """
        try:
            markerpositions_df = self._get_markerpositions(
                data_reader, variantset_ids, mapset_id)

            markerpositions_df = (
                markerpositions_df.fillna(value="").applymap(str))

            markerpositions_df = markerpositions_df[
                ["variantName", "linkageGroupName", "position"]]

            markerpositions_file_path = settings.get_maps_file_path(
                self.task_id, mapset_id)

            utils.df_to_csv(markerpositions_df,
                            markerpositions_file_path,
                            False,
                            False)
            return markerpositions_file_path

        except Exception:
            raise ValueError("Invalid or Empty Map.")

    def __get_dataset_without_split(self,
                                    consensus_task_id,
                                    filter_task_id,
                                    variantset_ids,
                                    brapi_base_url,
                                    auth_token):
        consensus_dataset_ = None

        try:
            if consensus_task_id is not None:
                consensus_data_reader = ConsensusDataReader(consensus_task_id)
                consensus_genotypes, consensus_callsets = (
                    consensus_data_reader.get_consensus_genotypes(),
                    consensus_data_reader.get_consensus_callsets())
                if consensus_genotypes is None or consensus_callsets is None:
                    raise RuntimeError("Unable to read consensus data")
                consensus_dataset_ = dataset(
                    genotype_data=consensus_genotypes,
                    sample_data=consensus_callsets)

            data_reader = FilterDataReader(brapi_base_url=brapi_base_url,
                                           auth_token=auth_token,
                                           filter_task_id=filter_task_id)

            genotypes_df = self._get_genotypes_df(data_reader, variantset_ids)

            callsets_df = self._get_callsets_df(data_reader, variantset_ids)

            dataset_ = dataset(genotype_data=genotypes_df,
                               sample_data=callsets_df,
                               sep=common.GENOTYPES_DATA_SPECS.SEPERATOR_CHAR,
                               mis=common.GENOTYPES_DATA_SPECS.MISSING_CHAR)

            if consensus_dataset_ is not None:
                dataset_ = consensus_dataset_.concat(dataset_)

            dataset_.export_to_file_fj_format(
                os.path.join(common.get_task_dir(self.task_id), "dataset"))

            project_file_datasets = {
                "dataset": "dataset"
            }

            return project_file_datasets
        except RuntimeError:
            raise RuntimeError("Unable to get dataset")

    def __get_datasets_with_split(self,
                                  split_columns,
                                  consensus_task_id,
                                  filter_task_id,
                                  variantset_ids,
                                  brapi_base_url,
                                  auth_token,
                                  project_file_num_parents=-1):

        consensus_dataset_ = None

        parent_columns = None
        parent_search_column = None
        parent_dataset_ = None
        missing = common.GENOTYPES_DATA_SPECS.MISSING_VALUE

        if consensus_task_id is not None:

            self._update_task_status("SPLIT-GENOTYPES-INPROGRESS",
                                     "reading consensus data")

            consensus_data_reader = ConsensusDataReader(consensus_task_id)

            consensus_dataset_ = dataset(
                genotype_data=consensus_data_reader.get_consensus_genotypes(),
                sample_data=consensus_data_reader.get_consensus_callsets())

        data_reader = FilterDataReader(brapi_base_url=brapi_base_url,
                                       auth_token=auth_token,
                                       filter_task_id=filter_task_id)

        genotypes_df = self._get_genotypes_df(data_reader, variantset_ids)

        callsets_df = self._get_callsets_df(data_reader, variantset_ids)

        if genotypes_df is None or callsets_df is None:
            raise ValueError("unable to read data")

        genotypes_df.fillna(missing, inplace=True)
        callsets_df = callsets_df.fillna("").applymap(str)

        parent_columns = ConsensusTask.get_parent_columns(
            callsets_df.columns.values)

        if len(parent_columns) > 0:
            parent_search_column = self.CONSENSUS_COLUMN

        dataset_ = dataset(genotype_data=genotypes_df,
                           sample_data=callsets_df,
                           parent_columns=parent_columns,
                           parent_search_column=parent_search_column,
                           sep=common.GENOTYPES_DATA_SPECS.SEPERATOR_CHAR,
                           mis=common.GENOTYPES_DATA_SPECS.MISSING_CHAR)

        if len(parent_columns) > 0:
            parent_dataset_ = dataset_.get_parent_data()

        splitter_ = splitter(dataset_)

        self._update_task_status("SPLIT-GENOTYPES-INPROGRESS",
                                 "splitting in progress")

        valid_split_columns = []

        if split_columns is not None:
            valid_split_columns = self.__get_valid_split_columns(
                split_columns,
                dataset_.sample_data.columns)

        if len(valid_split_columns) > 0:
            split_datasets = splitter_.split(*valid_split_columns)
        else:
            raise ValueError("Selected split columns not found in metadata")

        project_file_datasets, split_statistics = (
            self.__get_split_statistics(split_datasets,
                                        parent_columns,
                                        parent_dataset_,
                                        project_file_num_parents,
                                        consensus_dataset_,
                                        parent_search_column))

        return project_file_datasets, split_statistics

    def __get_split_statistics(self,
                               split_datasets,
                               parent_columns,
                               parent_dataset_,
                               project_file_num_parents,
                               consensus_dataset_=None,
                               parent_search_column="germplasmName"):

        project_file_datasets = {}

        datasets_by_num_parents = {}

        split_statistics = {
            "totalSplitDatasets": len(split_datasets),
            "projectFileDatasets": 0,
            "datasetsStatsByParentsCount": datasets_by_num_parents
        }

        datasets_with_replicates = {
            "datasets": [],
            "pedverAdvice": ("Please do Consensus Calling for "
                             "Flapjack batch processing to be successful")
        }

        dataset_index = 0

        for split_dataset_name, split_dataset in split_datasets.items():

            num_of_parents = 0
            dataset_index += 1

            if parent_columns is not None and len(parent_columns) > 0:

                split_datasets_parents = {}

                split_dataset.set_parents(parent_columns)

                # Gets the parent data in split dataset
                if consensus_dataset_ is not None:

                    split_datasets_parents[split_dataset_name] = (
                        consensus_dataset_.filter_for_parents_of_y(
                            split_dataset, parent_search_column))

                else:

                    split_datasets_parents[split_dataset_name] = (
                            parent_dataset_.filter_for_parents_of_y(
                                split_dataset, parent_search_column))

                num_of_parents = len(
                    split_datasets_parents[
                        split_dataset_name].genotype_data.index)

                split_datasets_parents[split_dataset_name].concat(
                        split_dataset)

                split_datasets[split_dataset_name] = (
                        split_datasets_parents[split_dataset_name])
                split_datasets[split_dataset_name].sample_data = (
                    split_datasets[split_dataset_name]
                    .sample_data.fillna(value="")
                    .applymap(str))

            # dataset stats for datasets grouped by num of parents in them
            if num_of_parents not in datasets_by_num_parents:
                datasets_by_num_parents[num_of_parents] = {
                    "datasetsCount": 0,
                    "datasets": []
                }

            dataset_stat = datasets_by_num_parents[num_of_parents]

            dataset_stat["datasetsCount"] += 1

            if split_dataset_name != "nan":
                dataset_stat["datasets"].append(split_dataset_name)
            else:
                dataset_stat["datasets"].append("undefined")

            if (project_file_num_parents >= 0 and
                    num_of_parents != project_file_num_parents):
                dataset_stat["pedverAdvice"] = (
                    "Not Included in Project file")

            if (project_file_num_parents < 0 or
                    num_of_parents == project_file_num_parents):

                split_dataset_file_name = "dataset_" + str(dataset_index)

                split_dataset_file_path = os.path.join(
                    common.get_task_dir(self.task_id),
                    split_dataset_file_name)

                split_datasets[split_dataset_name].export_to_file_fj_format(
                    split_dataset_file_path)

                project_file_datasets[split_dataset_name] = (
                    split_dataset_file_name)

                split_statistics["projectFileDatasets"] += 1

            if len(datasets_with_replicates["datasets"]) > 0:
                split_statistics["datasetsWithReplicates"] = (
                    datasets_with_replicates)
            completion_msg = split_dataset_name + ": splitting completed"

            self.completed_tasks.append({
                "task": "SPLIT-GENOTYPES",
                "status": completion_msg
            })

        return project_file_datasets, split_statistics

    def __create_project_file(self,
                              project_file_datasets,
                              markerpositions_file=None):

        self._update_task_status("SPLIT-GENOTYPES-INPROGRESS",
                                 "creating project file")

        splitter.batch_create_fj(
            common.get_task_dir(self.task_id),
            project_file_datasets,
            separator=common.GENOTYPES_DATA_SPECS.SEPERATOR_CHAR,
            missing=common.GENOTYPES_DATA_SPECS.MISSING_CHAR,
            mapfile=markerpositions_file,
            jarfile="flapjackjars/flapjack.jar",
            header=False)

        file_download_url = url_template.file_download_url.format(
            task_id=self.task_id,
            file_name=settings.OUTPUT_PROJECT_FILE_NAME)

        status_msg = {
            "status": "project file creation completed",
            "fileDownloadUrl":  file_download_url
        }

        self.completed_tasks.append({
            "task": "SPLIT-GENOTYPES",
            "status": status_msg
        })

        return file_download_url

    def __get_valid_split_columns(self, split_columns, dataset_columns):
        """ Returns valid split column
        """

        valid_split_columns = []

        additional_fields_map = common.USER_FIELD_TO_BRAPI_ADDITIONAL_FIELD_MAP

        def is_valid_additional_field(field):
            return (field in additional_fields_map and
                    additional_fields_map[field] in dataset_columns)

        for split_column in split_columns:
            if split_column in dataset_columns:
                valid_split_columns.append(split_column)
            elif is_valid_additional_field(split_column):
                valid_split_columns.append(
                    additional_fields_map[split_column])
        return valid_split_columns


@app.task(name="tasks.split_genotypes", bind=True)
def splitter_task(self, **kwargs):
    SplitGenotypesTask(self).run(**kwargs)
