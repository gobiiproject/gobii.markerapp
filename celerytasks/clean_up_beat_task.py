import os
import time
import shutil

from .celery import app

import common
import celerytasks.settings as settings


@app.task(name="tasks.daily_clean_up")
def daily_clean_up():
    """Beat task to removed task directories older than a day.
    """
    older_by_oneday_time = time.time() - (24*3600)

    # Get active task ids to not to clean up their folders
    active_task_ids = set()
    task_inspector = app.control.inspect()
    active_tasks = task_inspector.active()
    for task_server in active_tasks:
        for active_task in active_tasks[task_server]:
            if not active_task["name"] == "tasks.daily_clean_up":
                active_task_ids.add(active_task["id"])

    # Deletes folder for the tasks that are not active and older than 24 hours
    for _file_or_dir in os.scandir(common.ROOT_FILE_PATH):
        if (_file_or_dir.is_dir()
                and _file_or_dir.path != settings.CACHE_DIR
                and os.path.getmtime(_file_or_dir) < older_by_oneday_time
                and _file_or_dir not in active_task_ids):
            shutil.rmtree(_file_or_dir.path, ignore_errors=True)
