import os
import sys

import common

from .celery import app
from celery.exceptions import Ignore

import celerytasks.settings as settings
import celerytasks.utils as utils
import celerytasks.url_template as url_template

from celerytasks.pedver_task import PedverTask

from .data_reader import DataReader

from .f1pedver import dataset


class FilterGenotypesTask(PedverTask):

    def __init__(self, task):

        super().__init__(task)

        self.__genotypes_file_path = settings.filtered_genotypes_file_path(
            self.task_id)

        self.__callsets_file_path = settings.filtered_callsets_file_path(
            self.task_id)

        self.__preview_file_path = settings.filterd_output_preview_path(
                self.task_id)

        self.__file_download_url = url_template.file_download_url.format(
            task_id=self.task_id,
            file_name=settings.FILTERED_GENOTYPES_FILE_NAME)

        self.__sample_metadata_download_url = (
            url_template.file_download_url.format(
                task_id=self.task_id,
                file_name=settings.FILTERED_CALLSETS_FILE_NAME))

        self.__preview_file_url = url_template.preview_file_url.format(
            task_id=self.task_id,
            file_name=settings.FILTERED_GENOTYPES_FILE_NAME)

    def run(self,
            brapi_url=None,
            auth_token=None,
            variantset_ids=None,
            marker_call_rate=0,
            sample_call_rate=0):

        try:

            missing = common.GENOTYPES_DATA_SPECS.MISSING_VALUE

            data_reader = DataReader(brapi_base_url=brapi_url,
                                     auth_token=auth_token)

            genotypes_df = self._get_genotypes_df(data_reader, variantset_ids)

            callsets_df = self._get_callsets_df(data_reader, variantset_ids)

            if genotypes_df is None or callsets_df is None:
                raise ValueError("unable to read data")

            genotypes_df.fillna(missing, inplace=True)
            callsets_df = callsets_df.fillna("").applymap(str)

            self._update_task_status("FILTER-GENOTYPES-INPROGRESS",
                                     "filtering genotypes")

            dataset_ = dataset(genotype_data=genotypes_df,
                               sample_data=callsets_df,
                               sep=common.GENOTYPES_DATA_SPECS.SEPERATOR_CHAR,
                               mis=common.GENOTYPES_DATA_SPECS.MISSING_CHAR)

            filtered_dataset_ = dataset_.filter_genotype(
                sample_call_rate=sample_call_rate,
                marker_call_rate=marker_call_rate)

            total_samples_count, total_markers_count = (
                dataset_.genotype_data.shape[0],
                dataset_.genotype_data.shape[1])

            filtered_samples_count = (total_samples_count -
                                      filtered_dataset_.genotype_data.shape[0])

            filtered_markers_count = (total_markers_count -
                                      filtered_dataset_.genotype_data.shape[1])

            # Create a preview df
            preview_df = filtered_dataset_.genotype_data.iloc[0:50, 0:50]

            self._update_task_status("FILTER-GENOTYPES-INPROGRESS",
                                     "saving filtered dataset")

            filtered_dataset_.export_genotype_data_to_file(
                self.__genotypes_file_path)

            filtered_dataset_.export_sample_data_to_file(
                self.__callsets_file_path)

            utils.df_to_csv(preview_df, self.__preview_file_path)

            completion_msg = {
                "fileDownloadUrl":  self.__file_download_url,
                "filePreviewUrl": self.__preview_file_url,
                "sampleMetaDataUrl": self.__sample_metadata_download_url,
                "filteredMarkersCount": filtered_markers_count,
                "filteredSamplesCount":  filtered_samples_count,
                "totalMarkersCount": total_markers_count,
                "totalSamplesCount": total_samples_count
            }

            self.completed_tasks.append({
                "task": "FILTER-GENOTYPES",
                "status": completion_msg
            })

            self._update_task_status("FILTER-GENOTYPES-COMPLETED",
                                     completion_msg)

        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            self._update_task_status("FILTER-GENOTYPES-FAILED",
                                     "system error")
            raise Ignore()
        raise Ignore()

    def __genotypes_file_path(self):
        return settings.filtered_genotypes_file_path(self.task_id)

    def __callsets_file_path(self):
        return settings.filtered_callsets_file_path(self.task_id)


@app.task(name="tasks.filter_task", bind=True)
def filter_task(self, **kwargs):
    FilterGenotypesTask(self).run(**kwargs)
