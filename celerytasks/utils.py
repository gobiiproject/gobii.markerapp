#!/usr/bin/python

import os
import inspect
import logging
import sys

import json

import pandas as pd

import hashlib

import common


def get_file_as_df(file_path):
    """ Reads file as pandas dataframe and returns the same."""

    try:
        if (os.path.exists(file_path)):

            df = pd.read_csv(file_path,
                             sep=common.FILE_FORMAT.SEPERATOR_CHAR,
                             index_col=0)

            return df
        else:
            raise Exception("Unable to read file %s as df" % file_path)
    except OSError:
        raise Exception("Unable to read file %s as df" % file_path)


def df_to_csv(df, file_path, header=True, index=True):
    """ Writes datafeame as csv file in given file path"""
    try:
        df.to_csv(file_path,
                  index=index,
                  na_rep=common.FILE_FORMAT.FILL_NAN_CHAR,
                  sep=common.FILE_FORMAT.SEPERATOR_CHAR,
                  mode="w",
                  header=header,
                  line_terminator=common.FILE_FORMAT.LINE_TERMINATOR)
    except OSError:
        raise Exception("Unable to wrte df as file %s" % file_path)


def dict_to_json(dict_, file_path):
    try:
        with open(file_path, "w") as f_:
            json.dump(dict_, f_)
    except OSError:
        raise Exception("Unable to wrte dict as json file %s" % file_path)


def md5_hash(content):
    """ Returns md5 hash of given content"""
    try:
        if content is not None:
            return hashlib.md5(content.encode('utf')).hexdigest()
        else:
            raise ValueError("None values cannot hashed.")
    except Exception:
        raise Exception("Unable to do md5 hash.")


def log(message):
    """
    """
    func = inspect.currentframe().f_back.f_code
    exc_type, exc_obj, exc_tb = sys.exc_info()
    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
    print(exc_type, fname, exc_tb.tb_lineno)
    logging.warning("%s: %s in %s:%i", message, func.co_name,
                    func.co_filename, func.co_firstlineno)
