import unittest
from unittest.mock import Mock, patch

from celery.exceptions import Ignore

import pandas as pd

from celerytasks.split_genotypes_task import SplitGenotypesTask
from celerytasks.data_reader import DataReaderProgress


class SplitGenotypesTaskTest(unittest.TestCase):

    def setUp(self):
        self.task = Mock()
        self.task.request.id = "test_task_id"
        self.task.update_state.side_effect = self.update_task_status
        self.split_task = SplitGenotypesTask(self.task)

    def update_task_status(self, state="", meta={}):
        self.task.state = state
        self.task.meta = meta

    def get_test_genotypes(self):
        return pd.DataFrame({
            "A": ["GG", "AA", "CT", "TT", "TT"],
            "B": ["GG", "AA", "CT", "TT", "TT"],
            "C": ["GG", "AA", "CT", "TT", "TT"],
            "D": ["GG", "AA", "CT", "TT", "TT"],
            "E": ["GG", "AA", "CT", "TT", "TT"]
        }, index=["M1", "M2", "M3", "M4", "M5"])

    def get_test_callsets(self):
        return pd.DataFrame({
            "callSetName": ["A", "B", "C", "D", "E"],
            "sampleName": ["S1", "S1", "S2", "S2", "S3"],
            "germplasmName": ["G1_G2", "G1", "G2", "G3", "G2_G3"],
            "par1": ["G1", "", "", "", "G2"],
            "par2": ["G2", "", "", "", "G3"]
        })

    @patch('celerytasks.data_reader.FilterDataReader.get_callsets')
    @patch('celerytasks.data_reader.FilterDataReader.get_genotypes')
    def test_split_task_without_split(self,
                                      get_genotypes_mock,
                                      get_callsets_mock):
        get_genotypes_mock.return_value = [
            DataReaderProgress("d", "d", self.get_test_genotypes())]
        get_callsets_mock.return_value = [
            DataReaderProgress("d", "d", self.get_test_callsets())]
        with self.assertRaises(Ignore) as ce:
            self.split_task.run(brapi_url="dddd")
        self.assertTrue(self.split_task.task.state.endswith("COMPLETED"))

    @patch('celerytasks.data_reader.FilterDataReader.get_callsets')
    @patch('celerytasks.data_reader.FilterDataReader.get_genotypes')
    def test_split_task_with_split(self,
                                   get_genotypes_mock,
                                   get_callsets_mock):
        get_genotypes_mock.return_value = [
            DataReaderProgress("d", "d", self.get_test_genotypes())]
        get_callsets_mock.return_value = [
            DataReaderProgress("d", "d", self.get_test_callsets())]
        with self.assertRaises(Ignore) as ce:
            self.split_task.run(brapi_url="dddd",
                                split_columns=["germplasmPar1",
                                               "germplasmPar2"])
        print(self.split_task.task.state)
        self.assertTrue(self.split_task.task.state.endswith("COMPLETED"))



if __name__ == '__main__':
    unittest.main()
