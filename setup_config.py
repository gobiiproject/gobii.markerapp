#!/usr/bin/env python

import os
import sys
import argparse

import configparser

REDIST_IP="redis://{redis_ip}:6379/0"

try:
    parser = argparse.ArgumentParser()
    
    parser.add_argument("-i", "--inifile", 
            help="Config file to setup", required=True)
    
    parser.add_argument("-r", "--redisip", 
            help="redis ip", default="localhost")
    
    args = parser.parse_args()
    
    
    config = configparser.ConfigParser()
    config.optionxform = str
    
    config.read(args.inifile)
    
    config["CELERY"]["CELERY_BROKER_URL"] = REDIST_IP.format(redis_ip=args.redisip)
    config["CELERY"]["CELERY_RESULT_BACKEND"] = REDIST_IP.format(redis_ip=args.redisip)

    with open(args.inifile, 'w') as configfile:
        config.write(configfile)

except Exception as e:
    exc_type, exc_obj, exc_tb = sys.exc_info()
    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
    print(exc_type, fname, exc_tb.tb_lineno)

