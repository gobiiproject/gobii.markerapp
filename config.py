#!/usr/bin/python

import os
import configparser


def get_config():
    # Read Configuration
    config = configparser.ConfigParser()
    try:
        if os.path.exists("instance/config.ini"):
            config.read("instance/config.ini")
        elif os.path.exists("config.ini"):
            config.read("config.ini")
        else:
            raise RuntimeError("Configuaration file does not exists")
    except:
        raise RuntimeError("Unable to read celery configuration settings.")
    return config
