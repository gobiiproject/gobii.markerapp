#### Required Technologies

- [Docker](https://docs.docker.com/install/)
- [Python](https://www.python.org/downloads/)

Below bash shell commands are for Linux environment.
Set the environment variable HOST_DATA_VOLUME, the directory where the data files for the  
application needs to be created in host machine.

```

git clone https://bitbucket.org/gobiiproject/gobii.markerapp.git

cd gobii.markerapp

#git checkout {branch you are deploying}

mkdir ${HOST_DATA_VOLUME}

mkdir ${HOST_DATA_VOLUME}/data

mkdir ${HOST_DATA_VOLUME}/instance

export CONTAINER_DATA_FOLDER=/data/

export CONTAINER_INSTANCE_FOLDER=/gobiimarkerapp/instance/

# Pull Redis Image
docker pull redis

# Run Redis container
docker run --rm -dit -p 6379:6379 --name redis-node redis

# Setup config file
cp config.ini ${HOST_DATA_VOLUME}/instance/

REDIS_IP=$(docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' redis)

# Setup config.ini file
python2 setup_config.py -i ${HOST_DATA_VOLUME}/instance/config.ini -r ${REDIS_IP}

# Build Web Image
docker build -f gobiimarkerapp/Dockerfile -t gobiimarkerapp:1.5 .

# Build Worker Image
docker build -f celerytasks/Dockerfile -t gobiimarkerapp-worker:1.5 .

# Run Web Container
docker run --rm -dit \
-p 5000:80 \
-v ${HOST_DATA_VOLUME}/data:${CONTAINER_DATA_FOLDER} \
-v ${HOST_DATA_VOLUME}/instance:${CONTAINER_INSTANCE_FOLDER} \
--hostname gobiimarkerapp-web-node \
--name gobiimarkerapp \
gobiimarkerapp:1.5

# Run Worker Container
docker run -d --rm -i -t  \
-v ${HOST_DATA_VOLUME}/data:${CONTAINER_DATA_FOLDER} \
-v ${HOST_DATA_VOLUME}/instance:${CONTAINER_INSTANCE_FOLDER} \
--hostname gobiimarkerapp-worker-node  \
--name gobiimarkerapp-worker \
gobiimarkerapp-worker:1.5

```


