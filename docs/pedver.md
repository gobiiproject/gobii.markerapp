# F1 Pedigree Verification

Flapjack Pedigree Verification is a tool to Filter, Consensus call and Split genotypes, which are downloaded using **GOBii**'s (Genomics Open source Breeding Informatics Initiative) **BrAPI** (Breeding Application Programming Interface) complaint web services. Application creates Flapjack Project file to aide the breeders of different crops to achieve forward breeding and marker assisted backcrossing.

## Architecture:

The application has two components, Web application and an Analytics pipeline.

**Web Application** to get user input and to send jobs to the analytics pipeline.

**Analytics Pipeline** to run jobs for genotypes filtering, consensus calling and genotypes splitting.

Like any web application, there were two options on how to process the submitted tasks, Synchronous processing, or Asynchronous processing. For this application, there is no guarantee that the dataset selected by the user can be processed in given time. Since, the dataset may take more time to get processed, any interruption like network disconnection or the time taken by the task is beyond the limits of connection infrastructure may cause the submitted task to fail resulting user having to submit the process again. Keeping this in mind, Asynchronous processing is chosen over synchronous processing.

JavaScript, HTML, CSS are the frontend browser side programming languages.

Python is the server side backed programming language.

![F1 Pedver Architecture](images/PedVerArch.png)

### Presentation Layer (Front End):

The presentation layer is modern single-page application built using [Vue.js](https://vuejs.org/) framework. Vue.js was chosen, keeping in mind the limited time available to complete the MVP (Minimum Viable Product). Vue.js is a highly popular front-end framework ([167k GitHub star rating](https://github.com/vuejs/vue)) with less boiler plate code when compared to other framework like react.js or Angular. Unlike, React or Angular, which requires developers to learn or know technologies like JSX or Typescript respectively, Vue.js only requires the knowledge of JavaScript, HTML and

CSS, three core technologies of world wide web. Application can run in modern browsers like, Google Chrome, Mozilla Firefox, Internet Explorer, and any chromium-based browsers.

Node.js is server-side build tool which is used to build the front-end components. NPM is the package manager for the same.

Application consists of five components namely, Get Data, Filter, Consensus, Split and Export. Their descriptions are as follows,

**Get Data** is thepart where users select the Dataset to be processed. It has three dropdown inputs, a preview button, and a next page button.

The first dropdown input is to select Study (Experiment in Gobii terminology). Second one is to select Dataset. Third one is to select Genome maps. The Datasets and Genome Maps gets filtered by the selected study. Dataset input is a required input.

Preview Button is show preview of selected data. When clicked, a data table with genotypes in selected dataset will be shown to the user.

_Figure 2: Get Data page where user select dataset to process._

_Figure 3: Preview of the selected dataset_

**Filter** is the part where users filters the genotypes selected in Get Data page. It consists of two input text boxes, an Apply button, a Download button, and a next page button.

First input is to filter by marker percentage greater than the given value. Second input is to filter by sample percentage greater than the given value. When Apply button is clicked, selected dataset is filtered, and preview of Resultant dataset is shown. Resultant dataset can be downloaded by clicking Download button.

_Figure 5: Filter dataset page with preview of resulting filtered dataset. Download button to download the same._

**Consensus** is the page where parent consensus is generated for parents in the dataset. The page consists of a text box input, an Apply button and a Download button. Text box input is to specify consensus threshold.

When user clicks Apply button, generate consensus task is submitted. Once the task is completed, preview of resulting consensus will be shown to the users. Dataset file with parent consensus can be downloaded by clicking Download button.

_Figure 5: Consensus Calling page with preview of parent consensus._

**Split** page sis where the dataset is split based on splitting parameters selected by the user. The Page consists of multi select box, with list of split parameters and an Apply button to apply the splitting. Clicking the Apply button will generate the output flapjack project file.

## **Web Service Layer:**

Web service layer handles the submission to task to the backend server. It is built using Flask python framework. Web Services are designed following REST (Representational State Transfer) architecture.

It delivers the components of presentation layer as root service (&quot;\&quot;).

Web service layer allow only requests originating from the components of same origin to avoid security vulnerabilities originating from cross origin scripts in the browser.

## **Job Queue:**

Job Queue is a data structure to queue jobs for analytics pipeline. Tasks submitted by the users using the web services are queued and distributed to the workers waiting for the task.

Celery, a distributed task queue is the primary job queue of the application.

Redis is used as the backend in memory database for the distributed queue. Celery can also be configured to use any other backend queues or databases like RabbitMQ, Amazon SQS, Postgres., etc.

## **Workers:**

Task workers executes or computes the submitted jobs which includes,

Filtering, Consensus Calling, and Genotypes Splitting for now.

They can be scaled up or down based on the workload.

Celery takes care of automatically assigning the tasks in the job queue to the available worker.

## **Authentication:**

Users are authenticated against the Gobii Database they choose. Users will be entering the Gobii Webservice URL they want to access, and front-end application will submit the request to authenticate the user against the URL they provide. If the authentication succeeds, they will be allowed to access the application, otherwise they will be not. It is also illustrated as part of data flow below,

##

## **Caching:**

The presentation layer posts the variant set Id selected by the user to the web service. If the variant set id has never been selected by any users before, server-side worker will download the variant set by accessing BrAPI 2.0 complaint Gobii Webservices. The downloaded variant set is saved to cache folder with variant set id name. When a user selects a VariantSet id that is already cached, they will be fetched from the cache folder.

**Limitations of current cache implementation** are, When the cached dataset turns dirty, meaning when the state of cached data is not same as the one available in parent resource, there is no way of refreshing it automatically. Implementation of HTTP ETag will resolve this issue in future. If the cache data become dirty, it is recommended cleaning the cached data folder manually. Need more solid implementation of mutual exclusion when same variant set is downloaded simultaneously by two users.

# **Data Flow:**

The Data flow in the application is initiated when the user enters the URL of Gobii Web Service they want to access and supply username and password to access the same. All data flow happens through RESTful web services. Below figure explains data flow only for successful operations,

_Fig2: Data Flow of Request and Response between PedVer Client, GDM Webservices and PedVer Server_

# **Container Architecture:**

As explained in Architecture section, application is modularized based on their functionality. Containerization helps to isolate modules that can be evolved and deployed independently. It also helps to add new modules or remove/replace modules.

For current developmental deployment, the application has 3 container nodes.

## **Web Node:**

Web container node hosts the web server. Web Server serves the UI components and the Web Services.

Container is built using Ubuntu 18.04 as base image.

Gunicorn, a standard web server for Flask deployment is used as web server.

Gunicorn can be configured to a load balancer, or reverse proxies like Nginx. Gunicorn is sufficient for now as the application mostly serves dynamic content.

## **Queue Node:**

Queue node is kept as separate container module, to easily replace it with any other queuing data structure when necessary.

In current developmental deployment, Redis is the backend for celery task queue. Though, any other standard queue backends can also be used as backend. For example, RabbitMQ or Amazon SQS.

Redis official docker image is being used to build the queue node.


Host Machine

_Fig3: Shows how different modules of the application are containerized to enable scaling and reusability._

## **Worker Node:**

Celery distributed queue assigns tasks to the available free worker nodes. The Worker node is where the analytics tasks of Filter genotypes, Consensus Calling and Split genotypes are done. There can be more than one worker nodes based on the workload.

Container is built using Ubuntu 18.04 as base image.

