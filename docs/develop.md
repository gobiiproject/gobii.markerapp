
# Developing GOBii MarkerApp

## To setup development environment

### Required Technologies

- [Docker](https://docs.docker.com/install/)
- [Python 2](https://www.python.org/downloads/)
- [NPM](https://docs.npmjs.com/cli/install)

Run below commands to setup development environment

Instructions below are for Linux shell command line. Respective windows and mac steps
will be added later.

```sh
git clone https://bitbucket.org/gobiiproject/gobii.markerapp.git
cd gobiimarkerapp/

python2 -m venv venv
source venv/bin/activate
pip2 install --upgrade pip
pip2 install -r requirements.txt

cd gobiimarkerapp/client/
npm install

cd ~
docker pull redis
```

## To run in development mode

Setup the config.py for the instance

```sh
cd ~
cd gobiimarkerapp

mkdir instance

cp config.ini instance/
```

Create a data directory for the app. for example, "/data/gobiimarkerapp/"

Update the config.ini file with redis and data directory configurations, which
might look like below,

```ini
[CELERY]
CELERY_BROKER_URL = "redis://localhost:6378/0"
CELERY_RESULT_BACKEND = "redis://localhost:6378/0"

[APP_DATA]
DATA_FILES_ROOT = "/data/gobiimarkerapp/"
```

Run the below commands to start the development application,

```sh
cd ~

docker run -d -p 6379:6379 redis

cd gobiimarkerapp/

celery -A celerytasks worker --concurrency=4 --loglevel=info

cd gobiimarkerapp/client

npm run build

cd ../..

export FLASK_APP=gobiimarkerapp/app.py
export FLASK_ENV=development

flask run
```

open *http://127.0.0.1:5000* in your browser to make sure app is running.
